package com.codenjoy.dojo.icancode.solver;

import java.util.LinkedList;
import java.util.Queue;
import java.util.StringJoiner;
import java.util.concurrent.atomic.AtomicBoolean;

import org.apache.commons.collections4.queue.CircularFifoQueue;

import com.codenjoy.dojo.client.Solver;
import com.codenjoy.dojo.client.WebSocketRunner;
import com.codenjoy.dojo.icancode.Main;
import com.codenjoy.dojo.icancode.asearch.ASearch;
import com.codenjoy.dojo.icancode.asearch.command.ICommand;
import com.codenjoy.dojo.icancode.asearch.command.Special;
import com.codenjoy.dojo.icancode.client.Board;
import com.codenjoy.dojo.icancode.client.Command;
import com.codenjoy.dojo.icancode.client.ExtendedBoard;
import com.codenjoy.dojo.icancode.memory.LongMemory;
import com.codenjoy.dojo.icancode.memory.Memory;
import com.codenjoy.dojo.icancode.memory.Player;
import com.codenjoy.dojo.icancode.snapshot.Snapshot;
import com.codenjoy.dojo.icancode.strategy.IStrategy;
import com.codenjoy.dojo.icancode.strategy.KeyboardStrategy;
import com.codenjoy.dojo.icancode.strategy.StrategyResult;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ASearchSolver implements Solver<Board> {

    private final LongMemory longMemory = new LongMemory();
    private final IStrategy strategy;
    // commands queue
    private final Queue<ICommand> queue = new LinkedList<>();
    private StrategyResult result = new StrategyResult("empty", Special.DO_NOTHING); // last result

    // blocking check
    private final AtomicBoolean running = new AtomicBoolean(false);

    // unstuck
    private int cantMove = 0; //suicide on block

    // logging
    private final CircularFifoQueue<String> prevBoards = new CircularFifoQueue<>(3);

    public ASearchSolver(IStrategy strategy) {
        this.strategy = strategy;
    }

    @Override
    public String get(Board board) {
        ExtendedBoard extendedBoard = (ExtendedBoard) board;
        try {
            // start
            running.set(true);
            long start = System.currentTimeMillis();
            // actions
            Memory memory = new Memory(extendedBoard, longMemory);
            Player me = longMemory.getMe();
            log.info("{} board:{}", me, extendedBoard.size());
            if (me.isAlive()) {
                if (queue.isEmpty()) {
                    ASearch search = new ASearch(memory);
                    this.result = strategy.result(search);
                    queue.addAll(this.result.getCommands());
                    if (!queue.isEmpty()) {
                        cantMove = 0;
                    } else if (!(strategy instanceof KeyboardStrategy)) {
                        cantMove++;
                        Snapshot.warn("\n" + extendedBoard.superToString());
                        if (cantMove >= Main.DEATH_ON_STUCK) {
                            log.error("Self destroy! Cant move {} turns", cantMove);
                            queue.add(Special.DIE);
                            snapshot(log(me, extendedBoard));
                        }
                    }
                }
            } else {
                // if I am dead, I loose.. We should log this
                log.error("Dead");
                snapshot(log(me, extendedBoard));
            }
            // end
            this.prevBoards.add(log(me, extendedBoard));
            ICommand retval = queue.poll();
            log.info("Returning [{}/{}/{}] time:{}, cantMove:{} queue:{}", retval, result.getId(),
                    result.getRoute() != null ? result.getRoute().getTo() : null, System.currentTimeMillis() - start,
                    cantMove, queue);
            if (retval != null) {
                return retval.getCommand().toString();
            } else {
                return Command.doNothing().toString();
            }
        } catch (Throwable t) {
            log.error("Error. ", t);
            log.error("Board. {}", extendedBoard.superToString());
            return Command.doNothing().toString();
        } finally {
            running.set(false);
        }
    }

    public String log(Player me, ExtendedBoard extendedBoard) {
        return me.toString() + "\n" + result.toString() + "\nQueue:" + queue.toString() + "\n"
                + extendedBoard.superToString();
    }

    private void snapshot(String last) {
        StringJoiner sb = new StringJoiner("\n", "\n", "");
        for (String prevBoard : prevBoards) {
            sb.add(prevBoard);
        }
        sb.add(last);
        Snapshot.error(sb.toString());
    }

    public static void connectClient(String url, Solver<Board> solver, boolean print) {
        WebSocketRunner.runClient(url, solver, new ExtendedBoard(!print));
    }
}
