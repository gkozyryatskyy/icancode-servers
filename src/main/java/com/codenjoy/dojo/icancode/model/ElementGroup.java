package com.codenjoy.dojo.icancode.model;

import java.util.Set;
import java.util.function.Function;

import com.codenjoy.dojo.icancode.asearch.model.Cell;
import com.codenjoy.dojo.icancode.memory.Memory;

import lombok.Getter;

@Getter
public enum ElementGroup {

    ME(m -> m.get(Elements.ROBO, Elements.ROBO_FALLING, Elements.ROBO_FLYING, Elements.ROBO_LASER)),
    // other robots
    OTHER_ROBOTS(m -> m.get(Elements.ROBO_OTHER, Elements.ROBO_OTHER_FALLING, Elements.ROBO_OTHER_FLYING,
            Elements.ROBO_OTHER_LASER)),
    // other robots
    OTHER_ALIVE_ROBOTS(m -> m.get(Elements.ROBO_OTHER, Elements.ROBO_OTHER_FLYING)),

    // lasers
    LASERS(m -> m.get(Elements.LASER_LEFT, Elements.LASER_RIGHT, Elements.LASER_UP, Elements.LASER_DOWN)),
    LASER_MACHINES(m -> m.get(Elements.LASER_MACHINE_CHARGING_LEFT, Elements.LASER_MACHINE_CHARGING_RIGHT,
            Elements.LASER_MACHINE_CHARGING_UP, Elements.LASER_MACHINE_CHARGING_DOWN)),
    LASER_MACHINES_READY(m -> m.get(Elements.LASER_MACHINE_READY_LEFT, Elements.LASER_MACHINE_READY_RIGHT,
            Elements.LASER_MACHINE_READY_UP, Elements.LASER_MACHINE_READY_DOWN)),

    // zombie
    ZOMBIE(m -> m.get(Elements.MALE_ZOMBIE, Elements.FEMALE_ZOMBIE));

    private final Function<Memory, Set<Cell>> func;

    ElementGroup(Function<Memory, Set<Cell>> func) {
        this.func = func;
    }
}
