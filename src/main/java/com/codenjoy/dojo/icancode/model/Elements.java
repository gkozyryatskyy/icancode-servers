package com.codenjoy.dojo.icancode.model;

/*-
 * #%L
 * Codenjoy - it's a dojo-like platform from developers to developers.
 * %%
 * Copyright (C) 2018 Codenjoy
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import static com.codenjoy.dojo.icancode.model.Elements.Layers.LAYER1;
import static com.codenjoy.dojo.icancode.model.Elements.Layers.LAYER2;
import static com.codenjoy.dojo.icancode.model.Elements.Layers.LAYER3;

import java.util.Arrays;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.function.BiFunction;
import java.util.function.Predicate;

import com.codenjoy.dojo.icancode.Main;
import com.codenjoy.dojo.icancode.asearch.model.Cell;
import com.codenjoy.dojo.icancode.asearch.command.Move;
import com.codenjoy.dojo.services.Dice;
import com.codenjoy.dojo.services.printer.CharElements;
import com.google.common.collect.ImmutableList;

import lombok.Getter;

/**
 * All possible elements on board.
 */
public enum Elements implements CharElements {
    // empty space where player can go
    EMPTY(LAYER2, '-', c -> true, true, (u) -> true), //
    FLOOR(LAYER1, '.', c -> true, true, (u) -> true), //

    // walls
    ANGLE_IN_LEFT(LAYER1, '╔', c -> false, false, (u) -> false), //
    WALL_FRONT(LAYER1, '═', c -> false, false, (u) -> false), //
    ANGLE_IN_RIGHT(LAYER1, '┐', c -> false, false, (u) -> false), //
    WALL_RIGHT(LAYER1, '│', c -> false, false, (u) -> false), //
    ANGLE_BACK_RIGHT(LAYER1, '┘', c -> false, false, (u) -> false), //
    WALL_BACK(LAYER1, '─', c -> false, false, (u) -> false), //
    ANGLE_BACK_LEFT(LAYER1, '└', c -> false, false, (u) -> false), //
    WALL_LEFT(LAYER1, '║', c -> false, false, (u) -> false), //
    WALL_BACK_ANGLE_LEFT(LAYER1, '┌', c -> false, false, (u) -> false), //
    WALL_BACK_ANGLE_RIGHT(LAYER1, '╗', c -> false, false, (u) -> false), //
    ANGLE_OUT_RIGHT(LAYER1, '╝', c -> false, false, (u) -> false), //
    ANGLE_OUT_LEFT(LAYER1, '╚', c -> false, false, (u) -> false), //
    SPACE(LAYER1, ' ', c -> false, false, (u) -> false), //

    // laser machine
    /**
     * singleMoveFunc at LASER_MACHINE_CHARGING used for not jump into laser machine that become charged and will shoot
     * you
     */
    LASER_MACHINE_CHARGING_LEFT(LAYER1, '˂', c -> false, true, (u) -> false, singleMoveFunc(Move.LEFT)), //
    LASER_MACHINE_CHARGING_RIGHT(LAYER1, '˃', c -> false, true, (u) -> false, singleMoveFunc(Move.RIGHT)), //
    LASER_MACHINE_CHARGING_UP(LAYER1, '˄', c -> false, true, (u) -> false, singleMoveFunc(Move.UP)), //
    LASER_MACHINE_CHARGING_DOWN(LAYER1, '˅', c -> false, true, (u) -> false, singleMoveFunc(Move.DOWN)), //

    // lase machine ready
    LASER_MACHINE_READY_LEFT(LAYER1, '◄', c -> false, true, (u) -> false, singleMoveFunc(Move.LEFT)), //
    LASER_MACHINE_READY_RIGHT(LAYER1, '►', c -> false, true, (u) -> false, singleMoveFunc(Move.RIGHT)), //
    LASER_MACHINE_READY_UP(LAYER1, '▲', c -> false, true, (u) -> false, singleMoveFunc(Move.UP)), //
    LASER_MACHINE_READY_DOWN(LAYER1, '▼', c -> false, true, (u) -> false, singleMoveFunc(Move.DOWN)), //

    // other stuff
    START(LAYER1, 'S', c -> true, true, (u) -> true), //
    EXIT(LAYER1, 'E', c -> true, true, (u) -> true), //
    HOLE(LAYER1, 'O', c -> false, true, (u) -> true), //
    BOX(LAYER2, 'B', c -> false, true, (u) -> u != null ? u : false), //
    ZOMBIE_START(LAYER1, 'Z', c -> true, true, (u) -> true), //
    GOLD(LAYER1, '$', c -> true, true, (u) -> true), //

    // your robot
    ROBO(LAYER2, '☺', c -> true, true, (u) -> true), // canShotThrough = true to calculate shot from my position
    ROBO_FALLING(LAYER2, 'o', c -> false, true, (u) -> true), //
    ROBO_FLYING(LAYER3, '*', c -> true, true, (u) -> true), //
    ROBO_LASER(LAYER2, '☻', c -> true, true, (u) -> true), //

    // other robot
    ROBO_OTHER(LAYER2, 'X', c -> true, true, (u) -> true, Cell::getDirectionInRadius), // canShotThrough = true to calculate shot
    ROBO_OTHER_FALLING(LAYER2, 'x', c -> false, true, (u) -> true), //
    ROBO_OTHER_FLYING(LAYER3, '^', c -> true, true, (u) -> true), //
    ROBO_OTHER_LASER(LAYER2, '&', c -> true, true, (u) -> true), //

    // laser
    LASER_LEFT(LAYER2, '←', c -> {
        if (c == null) {
            return true;
        } else {
            // do not go to direct laser
            return !Move.RIGHT.equals(c);
        }
    }, true, (u) -> true, singleMoveFunc(Move.LEFT)), //
    LASER_RIGHT(LAYER2, '→', c -> {
        if (c == null) {
            return true;
        } else {
            // do not go to direct laser
            return !Move.LEFT.equals(c)
                    // because → can hide ← from view
                    && !Move.RIGHT.equals(c);
        }
    }, true, (u) -> true, singleMoveFunc(Move.RIGHT)), //
    LASER_UP(LAYER2, '↑', c -> {
        if (c == null) {
            return true;
        } else {
            // do not go to direct laser
            return !Move.DOWN.equals(c);
        }
    }, true, (u) -> true, singleMoveFunc(Move.UP)), //
    LASER_DOWN(LAYER2, '↓', Objects::isNull // true on null, for correct pattern recognition,
            // false on others because ↓ laser hides all other lasers
            , true, (u) -> true, singleMoveFunc(Move.DOWN)), //

    // zombie
    // still we cant just through zombie because cell around zombie is not accessible
    FEMALE_ZOMBIE(LAYER2, '♀', c -> false, true, (u) -> true,
            zombieMoveFunc()), // canShotThrough = true to calculate shot
    MALE_ZOMBIE(LAYER2, '♂', c -> false, true, (u) -> true,
            zombieMoveFunc()), // canShotThrough = true to calculate shot
    ZOMBIE_DIE(LAYER2, '✝', c -> true, true, (u) -> true), //

    // perks
    UNSTOPPABLE_LASER_PERK(LAYER1, 'l', c -> true, true, (u) -> false), //
    DEATH_RAY_PERK(LAYER1, 'r', c -> true, true, (u) -> false), //
    UNLIMITED_FIRE_PERK(LAYER1, 'f', c -> true, true, (u) -> false), //

    // system elements, don't touch it
    FOG(LAYER1, 'F', c -> false, false, (u) -> false), //
    BACKGROUND(LAYER2, 'G', c -> false, false, (u) -> false); //

    public static class Layers {
        public final static int LAYER1 = 0;
        public final static int LAYER2 = 1;
        public final static int LAYER3 = 2;
    }

    private static volatile Dictionary<String, Elements> elementsMap;

    private final char ch;
    private final int layer;
    @Getter
    private final Predicate<Move> canGoThroughFunc;
    @Getter
    private final boolean canJumpThrough;
    @Getter
    private final Predicate<Boolean> canShotThroughFunc;
    @Getter // moving elements support. Return possible, next turn destinations
    private final BiFunction<Cell, Integer, Set<Cell>> movingFunc; // TODO add func that returns empty list or me?

    Elements(int layer, char ch, Predicate<Move> canGoThrough, boolean canJumpThrough,
            Predicate<Boolean> canShotThrough) {
        this(layer, ch, canGoThrough, canJumpThrough, canShotThrough, null);
    }

    Elements(int layer, char ch, Predicate<Move> canGoThrough, boolean canJumpThrough,
            Predicate<Boolean> canShotThrough, BiFunction<Cell, Integer, Set<Cell>> movingFunc) {
        this.layer = layer;
        this.ch = ch;
        this.canGoThroughFunc = canGoThrough;
        this.canJumpThrough = canJumpThrough;
        this.canShotThroughFunc = canShotThrough;
        this.movingFunc = movingFunc;
    }

    public static BiFunction<Cell, Integer, Set<Cell>> singleMoveFunc(Move command) {
        return (start, steps) -> {
            Cell next = start;
            for (int i = 0; i < steps; i++) {
                if (next != null) {
                    next = command.forceMove(next);
                } else {
                    break;
                }
            }
            if (next != null) {
                return Set.of(next);
            } else {
                return null;
            }
        };
    }

    public static BiFunction<Cell, Integer, Set<Cell>> zombieMoveFunc() {
        return (start, steps) -> {
            // move according to zombie speed, assuming zombie do not go back or something.
            // this should be ok because mostly we need this for steps = 1
            int move = (steps + Main.ZOMBIE_SPEED - 1) / Main.ZOMBIE_SPEED;
            return start.getDirectionInRadius(move);
        };
    }

    // -----------------------------------------------------------------------------------------------

    @Override
    public char ch() {
        return ch;
    }

    @Override
    public String toString() {
        return String.valueOf(ch);
    }

    public static Elements valueOf(char ch) {
        if (elementsMap == null) {
            makeElementsMap();
        }

        Elements result = elementsMap.get(String.valueOf(ch));

        if (result == null) {
            throw new IllegalArgumentException("No such element for '" + ch + "'");
        }

        return result;
    }

    private static void makeElementsMap() {
        elementsMap = new Hashtable<>();

        for (Elements el : Elements.values()) {
            elementsMap.put(el.toString(), el);
        }
    }

    public static List<Elements> getPerks() {
        return ImmutableList.<Elements>builder().add(UNSTOPPABLE_LASER_PERK)
                .add(DEATH_RAY_PERK)
                .add(UNLIMITED_FIRE_PERK)
                .build();
    }

    public static Elements getRandomPerk(Dice dice) {
        List<Elements> perks = getPerks();
        return perks.get(dice.next(perks.size()));
    }

    public int getLayer() {
        return layer;
    }

    // TODO duplicate logic with ElementsMapper but we cant add it to client because a lot of dependencies
    public static boolean isWall(Elements el) {
        return Arrays.asList(ANGLE_IN_LEFT, WALL_FRONT, ANGLE_IN_RIGHT, WALL_RIGHT, ANGLE_BACK_RIGHT, WALL_BACK,
                ANGLE_BACK_LEFT, WALL_LEFT, WALL_BACK_ANGLE_LEFT, WALL_BACK_ANGLE_RIGHT, ANGLE_OUT_RIGHT,
                ANGLE_OUT_LEFT, SPACE).contains(el);
    }

}
