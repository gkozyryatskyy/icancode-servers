package com.codenjoy.dojo.icancode.client;

import com.codenjoy.dojo.icancode.model.Elements;

public class ExtendedBoard extends Board {

    /**
     * used for not use toString when it is not needed. See {@link com.codenjoy.dojo.client.WebSocketRunner.ClientSocket#onMessage(String)}
     */
    private final boolean silent;

    public ExtendedBoard(boolean silent) {
        this.silent = silent;
    }

    public char[][][] extGetField() {
        return field;
    }

    public Elements extGetAt(int numLayer, int x, int y) {
        return getAt(numLayer, x, y);
    }

    @Override
    public String toString() {
        if (silent) {
            return "Layers " + countLayers() + "[" + getField().length + ":" + getField()[0].length + "]";
        } else {
            return super.toString();
        }
    }

    public String superToString() {
        return super.toString();
    }
}
