package com.codenjoy.dojo.icancode.strategy;

import com.codenjoy.dojo.icancode.asearch.ASearch;

public interface IStrategy {

    StrategyResult result(ASearch search);

}
