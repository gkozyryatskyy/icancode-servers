package com.codenjoy.dojo.icancode.strategy;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Queue;
import java.util.Set;
import java.util.TreeSet;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import com.codenjoy.dojo.icancode.asearch.ASearch;
import com.codenjoy.dojo.icancode.asearch.ASearchRoute;
import com.codenjoy.dojo.icancode.asearch.ASearchRouteFilters;
import com.codenjoy.dojo.icancode.asearch.command.Move;
import com.codenjoy.dojo.icancode.asearch.model.Cell;
import com.codenjoy.dojo.icancode.asearch.model.Connection;
import com.codenjoy.dojo.icancode.asearch.model.RouteFinder;
import com.codenjoy.dojo.icancode.model.Elements;

public class StrategyUtils {

    private static final List<Elements> ORDERED_PERKS = List.of(Elements.DEATH_RAY_PERK, Elements.UNLIMITED_FIRE_PERK,
            Elements.UNSTOPPABLE_LASER_PERK);

    public static Optional<ASearchRoute> goToGold(ASearch search, Cell me, int distance,
            List<Predicate<ASearchRoute>> filters) {
        List<Cell> gold_perks_exit = search.getMemory()
                .get(Elements.GOLD)
                .stream()
                // filter distance
                .filter(e -> e.getX() <= me.getX() + distance && e.getX() >= me.getX() - distance)
                .filter(e -> e.getY() <= me.getY() + distance && e.getY() >= me.getY() - distance)
                .collect(Collectors.toList());
        Queue<ASearchRoute> routes = search.findRoutes(me, () -> gold_perks_exit);
        return ASearchRouteFilters.apply(routes, filters);
    }

    public static Optional<ASearchRoute> goToDRPerk(ASearch search, Cell me, int distance,
            List<Predicate<ASearchRoute>> filters) {
        List<Cell> perk = search.getMemory()
                .get(Elements.DEATH_RAY_PERK)
                .stream()
                // filter distance
                .filter(e -> e.getX() <= me.getX() + distance && e.getX() >= me.getX() - distance)
                .filter(e -> e.getY() <= me.getY() + distance && e.getY() >= me.getY() - distance)
                .collect(Collectors.toList());
        Queue<ASearchRoute> routes = search.findRoutes(me, () -> perk);
        return ASearchRouteFilters.apply(routes, filters);
    }

    public static Optional<ASearchRoute> goToPerks(ASearch search, Cell me, int distance,
            List<Predicate<ASearchRoute>> filters) {
        List<Cell> perks = search.getMemory()
                .get(ORDERED_PERKS)
                .stream()
                // filter distance
                .filter(e -> e.getX() <= me.getX() + distance && e.getX() >= me.getX() - distance)
                .filter(e -> e.getY() <= me.getY() + distance && e.getY() >= me.getY() - distance)
                .collect(Collectors.toList());
        Queue<ASearchRoute> routes = search.findRoutes(me, () -> perks);
        return ASearchRouteFilters.apply(routes, filters);
    }

    public static Optional<ASearchRoute> goToExit(ASearch search, Cell me, List<Predicate<ASearchRoute>> filters) {
        Queue<ASearchRoute> routes = search.findRoutes(me, () -> search.getMemory().get(Elements.EXIT));
        return ASearchRouteFilters.apply(routes, filters);
    }

    @Deprecated // do not working with Memory.next1OtherRoboLasers filled
    public static Optional<ASearchRoute> goToStartWithAfk(ASearch search, Cell me, int onDistance,
            List<Predicate<ASearchRoute>> filters) {
        List<Cell> starts = search.getMemory()
                .get(Elements.START)
                .stream()
                // filter starts with AFK players
                .filter(e -> e.is(Elements.ROBO_OTHER))
                .flatMap(e -> e.getDirectionInRadius(onDistance).stream())
                .collect(Collectors.toList());
        Queue<ASearchRoute> routes = search.findRoutes(me, () -> starts);
        return ASearchRouteFilters.apply(routes, filters);
    }

    public static Optional<ASearchRoute> goToStart(ASearch search, Cell me, int onDistance,
            List<Predicate<ASearchRoute>> filters) {
        List<Cell> starts = search.getMemory().get(Elements.START).stream()
                // filter starts with AFK players
                .flatMap(e -> e.getDirectionInRadius(onDistance).stream()).collect(Collectors.toList());
        Queue<ASearchRoute> routes = search.findRoutes(me, () -> starts);
        return ASearchRouteFilters.apply(routes, filters);
    }

    public static Optional<ASearchRoute> goToPath(ASearch search, Cell me, List<Predicate<ASearchRoute>> filters) {
        Queue<ASearchRoute> routes = search.findRoutes(me, () -> search.getMemory().getMapBorders());
        return ASearchRouteFilters.apply(routes, filters);
    }

    public static Optional<ASearchRoute> goToBorder(ASearch search, Cell me, Move side,
            List<Predicate<ASearchRoute>> filters) {
        Set<Cell> borderCells = search.getMemory().getMapBorder(side);
        Set<Cell> retval = new HashSet<>(borderCells);
        // try to go to boxes, mb there is a path
        borderCells.stream()
                .filter(c -> c.is(Elements.BOX))
                .flatMap(e -> e.getDirectionInRadius(1).stream())
                .forEach(retval::add);
        Queue<ASearchRoute> routes = search.findRoutes(me, () -> retval);
        return ASearchRouteFilters.apply(routes, filters);
    }

    public static Optional<ASearchRoute> goToNearestSafe(ASearch search, Cell me, int distance,
            List<Predicate<ASearchRoute>> filters) {
        Queue<ASearchRoute> routes = search.findRoutes(me, () -> {
            return new TreeSet<>(me.getSquare(distance));
        });
        return ASearchRouteFilters.apply(routes, filters);
    }

    public static Optional<ASearchRoute> anyFree(ASearch search, Cell me) {
        Connection meConn = new Connection(me);
        return meConn.getConnections()
                .stream()
                .findAny()
                .map(e -> new ASearchRoute(me, e.getCell(), new RouteFinder.Route<>(List.of(meConn, e), 0)));
    }

}
