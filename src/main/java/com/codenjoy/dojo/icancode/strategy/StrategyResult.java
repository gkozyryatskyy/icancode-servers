package com.codenjoy.dojo.icancode.strategy;

import java.util.List;
import java.util.Objects;

import com.codenjoy.dojo.icancode.asearch.ASearchRoute;
import com.codenjoy.dojo.icancode.asearch.command.ICommand;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString(onlyExplicitlyIncluded = true, includeFieldNames = false)
public class StrategyResult {

    @ToString.Include
    private final String id;
    @ToString.Include
    private final ASearchRoute route;
    private final List<ICommand> commands;

    public StrategyResult(String id, ASearchRoute route) {
        this.id = id;
        if (route != null) {
            this.route = route;
            this.commands = Objects.requireNonNullElseGet(route.getNextCommands(), List::of);
        } else {
            this.route = null;
            this.commands = List.of();
        }
    }

    public StrategyResult(String id, List<ICommand> commands) {
        this.id = id;
        this.route = null;
        this.commands = Objects.requireNonNullElseGet(commands, List::of);
    }

    public StrategyResult(String id, ICommand command) {
        this(id, command != null ? List.of(command) : List.of());
    }
}
