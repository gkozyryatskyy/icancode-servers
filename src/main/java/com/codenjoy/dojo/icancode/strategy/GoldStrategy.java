package com.codenjoy.dojo.icancode.strategy;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.function.Predicate;

import com.codenjoy.dojo.client.WebSocketRunner;
import com.codenjoy.dojo.icancode.Main;
import com.codenjoy.dojo.icancode.asearch.ASearch;
import com.codenjoy.dojo.icancode.asearch.ASearchRoute;
import com.codenjoy.dojo.icancode.asearch.ASearchRouteFilters;
import com.codenjoy.dojo.icancode.asearch.command.Act;
import com.codenjoy.dojo.icancode.asearch.command.Move;
import com.codenjoy.dojo.icancode.asearch.command.Special;
import com.codenjoy.dojo.icancode.memory.Player;
import com.codenjoy.dojo.icancode.model.Elements;
import com.codenjoy.dojo.icancode.solver.ASearchSolver;

public class GoldStrategy implements IStrategy {

    public static final Set<Elements> ZOMBIE_ACT_TARGETS = Set.of(Elements.MALE_ZOMBIE, Elements.FEMALE_ZOMBIE);

    public static final int GOLD_TO_FINISH_COUNT = 7;
    public static final int GOLD_TO_STOP_SHOOT_COUNT = 10;

    // TODO move queue here? to be able to handle going to danger cells while doing pattern?
    //    private final Queue<ICommand> queue = new LinkedList<>();

    @Override
    public StrategyResult result(ASearch search) {
        Player me = search.getMemory().getLongMemory().getMe();
        if (!me.getCell().isDanger(null) && me.getRecharge() <= 0) {
            Act act = null;
            if (me.getGold() < GOLD_TO_STOP_SHOOT_COUNT) {
                act = KillerStrategy.shot(search, me, KillerStrategy.ACT_TARGETS, 4);
            } else {
                act = KillerStrategy.shot(search, me, ZOMBIE_ACT_TARGETS, 4);
            }
            if (act != null) {
                me.shot();
                return new StrategyResult("shot", act);
            }
        }
        List<Predicate<ASearchRoute>> filters = List.of();
        // perks are expires fast
        return StrategyUtils.goToDRPerk(search, me.getCell(), 4, List.of(ASearchRouteFilters.length(5)))
                .map(e -> new StrategyResult("goToDRPerk", e))
                // going to gold up to GOLD_TO_FINISH_COUNT
                .or(() -> goToGold(search, me).map(e -> new StrategyResult("goToGold10/20", e)))
                // going to gold if it is close enough
                .or(() -> goToNearGold(search, me, 5, 5).map(e -> new StrategyResult("goToNearGold4/4", e)))
                // go to exit if we have gold
                .or(() -> goToExitWithGold(search, me, filters).map(e -> new StrategyResult("goToExitWithGold", e)))
                // go to other side of the map
                .or(() -> tryToGoAroundTheMap(search, me, filters).map(e -> new StrategyResult("tryToGoAroundTheMap", e)))
                // go to exit in low levels
                .or(() -> StrategyUtils.goToExit(search, me.getCell(), filters)
                        .map(e -> new StrategyResult("goToExit", e)))
                // if there is nowhere to go, go anywhere if we are in danger, else stay to handle unstuck
                .or(() -> dodgeDanger(search, me, filters).map(e -> new StrategyResult("dodgeDanger", e)))
                // empty response
                .orElse(new StrategyResult("goldStrategyEmpty", Special.DO_NOTHING));
    }

    public Optional<ASearchRoute> goToGold(ASearch search, Player me) {
        if (me.getGold() >= GOLD_TO_FINISH_COUNT) {
            return Optional.empty();
        } else {
            return goToNearGold(search, me, 10, 20);
        }
    }

    public Optional<ASearchRoute> goToNearGold(ASearch search, Player me, int searchDistance, int maxTicks) {
        return StrategyUtils.goToGold(search, me.getCell(), searchDistance,
                List.of(ASearchRouteFilters.length(maxTicks)));
    }

    public Optional<ASearchRoute> goToExitWithGold(ASearch search, Player me, List<Predicate<ASearchRoute>> filters) {
        // if there is no gold, do not go to exit
        if (me.getGold() <= 0) {
            return Optional.empty();
        } else {
            return StrategyUtils.goToExit(search, me.getCell(), filters);
        }
    }

    public static Optional<ASearchRoute> tryToGoAroundTheMap(ASearch search, Player me,
            List<Predicate<ASearchRoute>> filters) {
        // try to go down the map in priority, when we are going around the map
        return StrategyUtils.goToBorder(search, me.getCell(), Move.UP, filters)
                .or(() -> StrategyUtils.goToBorder(search, me.getCell(), Move.LEFT, filters))
                .or(() -> StrategyUtils.goToBorder(search, me.getCell(), Move.RIGHT, filters))
                .or(() -> StrategyUtils.goToBorder(search, me.getCell(), Move.DOWN, filters));
    }

    public static Optional<ASearchRoute> dodgeDanger(ASearch search, Player me, List<Predicate<ASearchRoute>> filters) {
        if (me.getCell().isDanger(null)) {
            return StrategyUtils.goToNearestSafe(search, me.getCell(), 2, filters);
        } else {
            return Optional.empty();
        }
    }

    public static void main(String[] args) {
        WebSocketRunner.PRINT_TO_CONSOLE = Main.print;
        GoldStrategy strategy = new GoldStrategy();
        ASearchSolver.connectClient(
                String.format("https://epam-botchallenge.com/codenjoy-contest/board/player/%s?code=%s", Main.player,
                        Main.code), new ASearchSolver(strategy), Main.print);
    }
}
