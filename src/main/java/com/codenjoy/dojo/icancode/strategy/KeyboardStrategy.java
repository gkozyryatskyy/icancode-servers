package com.codenjoy.dojo.icancode.strategy;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import com.codenjoy.dojo.client.WebSocketRunner;
import com.codenjoy.dojo.icancode.Main;
import com.codenjoy.dojo.icancode.asearch.ASearch;
import com.codenjoy.dojo.icancode.asearch.command.ICommand;
import com.codenjoy.dojo.icancode.asearch.command.Jump;
import com.codenjoy.dojo.icancode.asearch.command.Move;
import com.codenjoy.dojo.icancode.asearch.command.Pull;
import com.codenjoy.dojo.icancode.asearch.command.Special;
import com.codenjoy.dojo.icancode.solver.ASearchSolver;

public class KeyboardStrategy extends Frame implements IStrategy, KeyListener {

    private final BlockingQueue<ICommand> queue = new LinkedBlockingQueue<>();
    private boolean pullMode = false; //Q
    private boolean jumpMode = false; //W

    private final Label l;
    private final TextArea area;

    public KeyboardStrategy() {
        this.l = new Label();
        this.l.setBounds(20, 50, 100, 20);
        this.area = new TextArea();
        this.area.setBounds(20, 80, 300, 300);
        this.area.addKeyListener(this);
        add(l);
        add(area);
        setSize(400, 400);
        setLayout(null);
        setVisible(true);
    }

    @Override
    public StrategyResult result(ASearch search) {
        java.util.List<ICommand> retval = new ArrayList<>();
        queue.drainTo(retval);
        return new StrategyResult("keyboard", retval);
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
        ICommand retval = null;
        switch (e.getKeyCode()) {
            case KeyEvent.VK_LEFT:
                if (pullMode) {
                    queue.add(Pull.LEFT);
                } else if (jumpMode) {
                    queue.add(Jump.LEFT);
                    queue.add(Special.DO_NOTHING);
                } else {
                    queue.add(Move.LEFT);
                }
                break;
            case KeyEvent.VK_RIGHT:
                if (pullMode) {
                    queue.add(Pull.RIGHT);
                } else if (jumpMode) {
                    queue.add(Jump.RIGHT);
                    queue.add(Special.DO_NOTHING);
                } else {
                    queue.add(Move.RIGHT);
                }
                break;
            case KeyEvent.VK_UP:
                if (pullMode) {
                    queue.add(Pull.UP);
                } else if (jumpMode) {
                    queue.add(Jump.UP);
                    queue.add(Special.DO_NOTHING);
                } else {
                    queue.add(Move.UP);
                }
                break;
            case KeyEvent.VK_DOWN:
                if (pullMode) {
                    queue.add(Pull.DOWN);
                } else if (jumpMode) {
                    queue.add(Jump.DOWN);
                    queue.add(Special.DO_NOTHING);
                } else {
                    queue.add(Move.DOWN);
                }
                break;
            case KeyEvent.VK_SPACE:
                queue.add(Jump.JUMP);
                queue.add(Special.DO_NOTHING);
                break;
            case KeyEvent.VK_Q:
                pullMode = !pullMode;
                break;
            case KeyEvent.VK_W:
                jumpMode = !jumpMode;
                break;
            case KeyEvent.VK_R:
                queue.add(Special.DIE);
                break;
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {

    }

    public static void main(String[] args) {
        WebSocketRunner.PRINT_TO_CONSOLE = Main.print;
        KeyboardStrategy strategy = new KeyboardStrategy();
        ASearchSolver.connectClient(
                String.format("https://epam-botchallenge.com/codenjoy-contest/board/player/%s?code=%s", Main.player,
                        Main.code), new ASearchSolver(strategy), Main.print);
    }
}
