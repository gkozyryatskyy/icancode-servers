package com.codenjoy.dojo.icancode.strategy;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.function.Predicate;

import com.codenjoy.dojo.client.WebSocketRunner;
import com.codenjoy.dojo.icancode.Main;
import com.codenjoy.dojo.icancode.asearch.ASearch;
import com.codenjoy.dojo.icancode.asearch.ASearchRoute;
import com.codenjoy.dojo.icancode.asearch.ASearchRouteFilters;
import com.codenjoy.dojo.icancode.asearch.command.Act;
import com.codenjoy.dojo.icancode.asearch.command.Move;
import com.codenjoy.dojo.icancode.asearch.command.Special;
import com.codenjoy.dojo.icancode.memory.Player;
import com.codenjoy.dojo.icancode.model.Elements;
import com.codenjoy.dojo.icancode.solver.ASearchSolver;

public class KillerStrategy implements IStrategy {

        public static final Set<Elements> ACT_TARGETS = Set.of(Elements.ROBO_OTHER, Elements.ROBO_OTHER_FLYING,
                Elements.MALE_ZOMBIE, Elements.FEMALE_ZOMBIE);

    @Override
    public StrategyResult result(ASearch search) {
        Player me = search.getMemory().getLongMemory().getMe();
        if (!me.getCell().isDanger(null) && me.getRecharge() <= 0) {
            Act act = KillerStrategy.shot(search, me, ACT_TARGETS,5);
            if (act != null) {
                me.shot();
                return new StrategyResult("shot", act);
            }
        }
        List<Predicate<ASearchRoute>> filters = List.of();
        // perks are expires fast
        return StrategyUtils.goToPerks(search, me.getCell(), 4, List.of(ASearchRouteFilters.length(6)))
                .map(e -> new StrategyResult("goToPerks", e))
                //                .or(() -> StrategyUtils.goToStartWithAfk(search, me.getCell(), 2, filters)
                //                        .map(e -> new StrategyResult("goToStartWithAfk2", e)))
                .or(() -> StrategyUtils.goToStartWithAfk(search, me.getCell(), 1, filters)
                        .map(e -> new StrategyResult("goToStartWithAfk1", e)))
                .or(() -> StrategyUtils.goToStart(search, me.getCell(), 2, filters)
                        .map(e -> new StrategyResult("goToStart", e)))
                // go to other side of the map
                .or(() -> tryToGoUPTheMap(search, me, filters).map(e -> new StrategyResult("tryToGoUPTheMap", e)))
                // if there is nowhere to go, go anywhere if we are in danger, else stay to handle unstuck
                .or(() -> GoldStrategy.dodgeDanger(search, me, filters).map(e -> new StrategyResult("dodgeDanger", e)))
                // empty response
                .orElse(new StrategyResult("goldStrategyEmpty", Special.DO_NOTHING));
    }

    public static Act shot(ASearch search, Player me, Set<Elements> targets, int distance) {
        if (me.getDeathRay() > 0) {
            distance = Main.PERKS_DEATH_RAY_LENGTH;
        }
        Integer targetDistance = null;
        Act command = null;
        for (Act c : Act.values()) {
            int temp = c.calculateShot(me.getCell(), distance, me.getUnstoppableLaser() > 0, targets);
            if (temp > 0 && (targetDistance == null || temp < targetDistance)) {
                targetDistance = temp;
                command = c;
            }
        }
        return command;
    }

    public static Optional<ASearchRoute> tryToGoUPTheMap(ASearch search, Player me,
            List<Predicate<ASearchRoute>> filters) {
        // try to go down the map in priority, when we are going around the map
        return StrategyUtils.goToBorder(search, me.getCell(), Move.UP, filters)
                .or(() -> StrategyUtils.goToBorder(search, me.getCell(), Move.RIGHT, filters))
                .or(() -> StrategyUtils.goToBorder(search, me.getCell(), Move.LEFT, filters))
                .or(() -> StrategyUtils.goToBorder(search, me.getCell(), Move.DOWN, filters));
    }

    public static void main(String[] args) {
        WebSocketRunner.PRINT_TO_CONSOLE = Main.print;
        KillerStrategy strategy = new KillerStrategy();
        ASearchSolver.connectClient(
                String.format("https://epam-botchallenge.com/codenjoy-contest/board/player/%s?code=%s", Main.player,
                        Main.code), new ASearchSolver(strategy), Main.print);
    }
}
