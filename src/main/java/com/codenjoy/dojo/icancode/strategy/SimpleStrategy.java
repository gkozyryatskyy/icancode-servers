package com.codenjoy.dojo.icancode.strategy;

import com.codenjoy.dojo.client.WebSocketRunner;
import com.codenjoy.dojo.icancode.Main;
import com.codenjoy.dojo.icancode.asearch.ASearch;
import com.codenjoy.dojo.icancode.asearch.command.Act;
import com.codenjoy.dojo.icancode.solver.ASearchSolver;

public class SimpleStrategy implements IStrategy {

    @Override
    public StrategyResult result(ASearch search) {
        return new StrategyResult("simple", Act.RIGHT);
    }

    public static void main(String[] args) {
        WebSocketRunner.PRINT_TO_CONSOLE = Main.print;
        SimpleStrategy strategy = new SimpleStrategy();
        ASearchSolver.connectClient(
                String.format("https://epam-botchallenge.com/codenjoy-contest/board/player/%s?code=%s", Main.player, Main.code),
                new ASearchSolver(strategy), Main.print);
    }
}
