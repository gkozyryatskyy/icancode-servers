package com.codenjoy.dojo.icancode;

import java.util.function.Predicate;

import com.codenjoy.dojo.icancode.asearch.model.Cell;
import com.codenjoy.dojo.icancode.strategy.KeyboardStrategy;

public class MainTest {

    public static void main(String[] args) {
        new KeyboardStrategy();
    }

    public static Predicate<Cell> any() {
        return (c) -> true;
    }

}
