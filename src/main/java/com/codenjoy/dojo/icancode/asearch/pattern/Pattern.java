package com.codenjoy.dojo.icancode.asearch.pattern;

import java.util.List;
import java.util.Objects;
import java.util.function.BiFunction;
import java.util.function.Predicate;

import com.codenjoy.dojo.icancode.asearch.command.ICommand;
import com.codenjoy.dojo.icancode.asearch.model.Cell;
import com.codenjoy.dojo.icancode.asearch.model.Connection;
import com.codenjoy.dojo.icancode.memory.Memory;

import lombok.Getter;

@Getter
public class Pattern {

    private final XY start;
    private final XY end;
    private final CellPredicate[][] cellPattern;
    private final Predicate<Cell[][]> fieldPattern;
    private final List<ICommand> commands;
    private final Double scoreAdd;

    protected Pattern(XY start, XY end, CellPredicate[][] cellPattern, Predicate<Cell[][]> fieldPattern,
            List<ICommand> commands, Double scoreAdd) {
        Objects.requireNonNull(start);
        Objects.requireNonNull(end);
        Objects.requireNonNull(cellPattern);
        Objects.requireNonNull(fieldPattern);
        Objects.requireNonNull(commands);
        this.start = start;
        this.end = end;
        this.cellPattern = cellPattern;
        this.fieldPattern = fieldPattern;
        this.commands = commands;
        this.scoreAdd = scoreAdd;
        checkCommands();
    }

    protected Pattern(XY start, XY end, CellPredicate[][] cellPattern, Predicate<Cell[][]> fieldPattern,
            List<ICommand> commands) {
        this(start, end, cellPattern, fieldPattern, commands, null);
    }

    private void checkCommands() {
        if (!end.equals(ICommand.forceMove(commands, start))) {
            throw new IllegalArgumentException("Commands do not lead from start to end.");
        }
    }

    public Connection match(Cell cell) {
        Memory memory = cell.getMemory();
        if (checkRotated((x, y) -> memory.get(getShiftedXY(new XY(x, y), cell, null)))) {
            // rotate 0
            return getConnection(cell, null);
        } else if (checkRotated((x, y) -> memory.get(getShiftedXY(new XY(x, y), cell, Rotate.ROTATE_90)))) {
            // rotate 90
            return getConnection(cell, Rotate.ROTATE_90);
        } else if (checkRotated((x, y) -> memory.get(getShiftedXY(new XY(x, y), cell, Rotate.ROTATE_180)))) {
            // rotate 180
            return getConnection(cell, Rotate.ROTATE_180);
        } else if (checkRotated((x, y) -> memory.get(getShiftedXY(new XY(x, y), cell, Rotate.ROTATE_270)))) {
            // rotate 270
            return getConnection(cell, Rotate.ROTATE_270);
        }
        return null;
    }

    private XY getShiftedXY(XY xy, Cell realStart, Rotate rotate) {
        // shift to pattern start corner
        xy = new XY(xy.x - start.x, xy.y - start.y);
        // rotate pattern
        if (rotate != null) {
            xy = rotate.rotateFunc().apply(xy);
        }
        // shift to real board coordinates
        return new XY(xy.x + realStart.getX(), xy.y + realStart.getY());
    }

    private boolean checkRotated(BiFunction<Integer, Integer, Cell> rotateFunc) {
        CellPredicate[][] pattern = getCellPattern();
        Cell[][] patternCells = new Cell[pattern.length][pattern[0].length];
        for (int x = 0; x < pattern.length; x++) {
            for (int y = 0; y < pattern[0].length; y++) {
                Cell cell = rotateFunc.apply(x, y);
                boolean match = pattern[x][y].test(cell);
                if (match) {
                    patternCells[x][y] = cell;
                } else {
                    return false;
                }
            }
        }
        return getFieldPattern().test(patternCells);
    }

    public Connection getConnection(Cell realStart, Rotate rotate) {
        List<ICommand> commands;
        if (rotate != null) {
            commands = rotate.rotateCommandsFunc().apply(getCommands());
        } else {
            commands = getCommands();
        }
        Cell end = realStart.getMemory().get(ICommand.forceMove(commands, new XY(realStart.getX(), realStart.getY())));
        return new Connection(end, commands, scoreAdd);
    }
}
