package com.codenjoy.dojo.icancode.asearch.command;

import java.util.List;

import com.codenjoy.dojo.icancode.asearch.model.Cell;
import com.codenjoy.dojo.icancode.asearch.model.Connection;
import com.codenjoy.dojo.icancode.asearch.pattern.XY;
import com.codenjoy.dojo.icancode.client.Command;

public interface ICommand {

    int getX();

    int getY();

    boolean isJump();

    Command getCommand();

    /**
     * Additional score to the distance between cells see {@link com.codenjoy.dojo.icancode.asearch.model.ConnectionScorer#computeCost(Connection,
     * Connection)}
     *
     * @return additional score for this command
     */
    double getScore();

    /**
     * re clockwise rotated value
     *
     * @return rotated value
     */
    ICommand rotated();

    /**
     * forceMove is a move, with out any checks. Anyway it can return null if mem.get() will return null
     *
     * @param cell Cell to move
     *
     * @return moved cell. null if mem.get() will return null
     */
    default Cell forceMove(Cell cell) {
        return cell.getMemory().get(cell.getX() + getX(), cell.getY() + getY());
    }

    default XY forceMove(XY xy) {
        return new XY(xy.x + getX(), xy.y + getY());
    }

    static XY forceMove(List<ICommand> commands, XY xy) {
        for (ICommand c : commands) {
            xy = c.forceMove(xy);
        }
        return xy;
    }

}
