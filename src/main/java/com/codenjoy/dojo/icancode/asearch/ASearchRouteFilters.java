package com.codenjoy.dojo.icancode.asearch;

import java.util.List;
import java.util.Optional;
import java.util.Queue;
import java.util.Set;
import java.util.function.Predicate;

import com.codenjoy.dojo.icancode.asearch.model.Cell;
import com.codenjoy.dojo.icancode.memory.Memory;
import com.codenjoy.dojo.icancode.model.Elements;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ASearchRouteFilters {

    public static Optional<ASearchRoute> apply(Queue<ASearchRoute> routes, List<Predicate<ASearchRoute>> filters) {
        return routes.stream().filter(filters.stream().reduce(x -> true, Predicate::and)).findAny();
    }

    public static Predicate<ASearchRoute> hasWayBack(ASearch search) {
        return (route) -> {
            // check if we have a way back, to any close cell not from the route
            Queue<ASearchRoute> back = search.findRoutes(route.getTo(), () -> {
                Set<Cell> backCells = route.getFrom().getDirection(1);
                // remove any rout cell from back cells
                backCells.removeAll(route.getRoute());
                return backCells;
            });
            if (!back.isEmpty()) {
                log.debug(route.toString());
                log.debug("way back:{}", back);
                return true;
            } else {
                return false;
            }
        };
    }

    /**
     * Exclude routes with specific elements
     *
     * @param mem current board Memory
     * @param exclude Elements to exclude
     *
     * @return filter
     */
    public static Predicate<ASearchRoute> noElementOnTheRoute(Memory mem, Set<Elements> exclude) {
        return (route) -> {
            return route.getRoute().stream().flatMap(e -> e.getE().stream()).noneMatch(exclude::contains);
        };
    }

    public static Predicate<ASearchRoute> length(int maxLength) {
        return (route) -> route.size() <= maxLength;
    }
}
