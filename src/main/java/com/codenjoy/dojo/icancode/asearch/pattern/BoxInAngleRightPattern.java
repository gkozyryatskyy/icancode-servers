package com.codenjoy.dojo.icancode.asearch.pattern;

import java.util.List;
import java.util.function.Predicate;

import com.codenjoy.dojo.icancode.asearch.command.ICommand;
import com.codenjoy.dojo.icancode.asearch.command.Jump;
import com.codenjoy.dojo.icancode.asearch.command.Pull;
import com.codenjoy.dojo.icancode.asearch.command.Special;
import com.codenjoy.dojo.icancode.asearch.model.Cell;
import com.codenjoy.dojo.icancode.model.Elements;

public class BoxInAngleRightPattern extends Pattern {

    // @formatter:off
    /**
     * pattern
     * . - any
     * X - cant jump through
     * g - can go through
     * B - box
     *
     *   |---|
     * 3 |.g.|
     * 2 |Xg.|
     * 1 |gBX|
     * 0 |.X.|
     *   |---|
     *    012
     */

    private static final CellPredicate[][] cellPattern = new CellPredicate[][] {
            { (c) -> true,
                    (c) -> c != null && c.isCanGoThrough(null),
                    (c) -> c != null && !c.isCanJumpThrough(),
                    (c) -> true }, // x0
            { (c) -> c != null && !c.isCanJumpThrough(), // x1
                    (c) -> c != null && c.is(Elements.BOX),
                    (c) -> c != null && c.isCanGoThrough(null),
                    (c) -> c != null && c.isCanGoThrough(null) },
            { (c) -> true, (c) -> c != null && !c.isCanJumpThrough(), (c) -> true, (c) -> true }}; // 2
    // @formatter:on

    private static final Predicate<Cell[][]> fieldPattern = (c) -> true;

    private static final List<ICommand> commands = List.of(Pull.UP, Jump.DOWN, Special.DO_NOTHING);

    public BoxInAngleRightPattern() {
        super(new XY(1, 2), new XY(1, 1), cellPattern, fieldPattern, commands);
    }
}