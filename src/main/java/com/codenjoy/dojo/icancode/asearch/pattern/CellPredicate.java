package com.codenjoy.dojo.icancode.asearch.pattern;

import java.util.function.Predicate;

import com.codenjoy.dojo.icancode.asearch.model.Cell;

public interface CellPredicate extends Predicate<Cell> {
}
