package com.codenjoy.dojo.icancode.asearch.command;

import com.codenjoy.dojo.icancode.asearch.model.Cell;
import com.codenjoy.dojo.icancode.client.Command;
import com.codenjoy.dojo.services.Direction;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Jump implements ICommand {

    JUMP(0, 0, null, Command.jump(), 2), //
    LEFT(-2, 0, Move.LEFT, Command.jump(Direction.LEFT), 1), //
    RIGHT(2, 0, Move.RIGHT, Command.jump(Direction.RIGHT), 1), //
    UP(0, 2, Move.UP, Command.jump(Direction.UP), 1), //
    DOWN(0, -2, Move.DOWN, Command.jump(Direction.DOWN), 1); //

    private final int x;
    private final int y;
    private final Move jumpDirection;
    private final Command command;
    private final double score;

    /**
     * @param cell Cell to move
     *
     * @return moved cell. null if mem.get() will return null or failed isCanGoThrough/isCanJumpThrough checks
     */
    public Cell move(Cell cell) {
        Cell retval = forceMove(cell);
        // do not return if we cant go there
        if (retval != null && retval.isCanGoThrough(null)) {
            if (jumpDirection != null) {
                Cell jumpThrough = jumpDirection.forceMove(cell);
                // do not return if we can not jump through previous cell
                if (jumpThrough != null && jumpThrough.isCanJumpThrough()) {
                    return retval;
                } else {
                    return null;
                }
            } else {
                return retval;
            }
        } else {
            return null;
        }
    }

    @Override
    public boolean isJump() {
        return true;
    }

    @Override
    public ICommand rotated() {
        switch (this) {
            case LEFT:
                return UP;
            case UP:
                return RIGHT;
            case RIGHT:
                return DOWN;
            case DOWN:
                return LEFT;
            case JUMP:
                return JUMP;
        }
        throw new IllegalArgumentException("Cant rotate.");
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + "." + name();
    }
}

