package com.codenjoy.dojo.icancode.asearch;

import java.util.Arrays;
import java.util.Collection;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Set;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import com.codenjoy.dojo.icancode.asearch.model.Cell;
import com.codenjoy.dojo.icancode.asearch.model.Connection;
import com.codenjoy.dojo.icancode.asearch.model.ConnectionScorer;
import com.codenjoy.dojo.icancode.asearch.model.Graph;
import com.codenjoy.dojo.icancode.asearch.model.RouteFinder;
import com.codenjoy.dojo.icancode.asearch.model.Scorer;
import com.codenjoy.dojo.icancode.memory.Memory;
import com.codenjoy.dojo.icancode.model.Elements;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ASearch {

    // this set is different from Elements.canGoThrough, because even if we can go through, do not mean that we want to
    public static final Set<Elements> ACCESSIBLE_ELEMENTS = Arrays.stream(Elements.values())
            .filter(e -> e.getCanGoThroughFunc().test(null) || e.isCanJumpThrough())
            .collect(Collectors.toSet());

    @Getter
    private final Memory memory;
    private final Graph<Connection> availableBoard;
    private final RouteFinder<Connection> routeFinder;

    public ASearch(Memory memory) {
        this.memory = memory;
        this.availableBoard = new Graph<>();
        this.routeFinder = new RouteFinder<>(availableBoard, new ConnectionScorer(), new ConnectionScorer());
    }

    protected ASearchRoute findRoute(Cell from, Cell to) {
        return findRoute(from, to, null);
    }

    protected ASearchRoute findRoute(Cell from, Cell to, Scorer<Connection> costumeScorer) {
        // check that this node in the accessible board
        if (from.equals(to) || !to.isCanGoThrough(null) || to.isDanger(null)) {
            return null;
        } else {
            RouteFinder.Route<Connection> route = routeFinder.findRoute(new Connection(from), new Connection(to),
                    costumeScorer);
            // init ASearchRoute for logging, even if it has null route
            if (route == null) {
                log.debug("Route from {} to {} is null", from, to);
                return null;
            } else {
                ASearchRoute retval = new ASearchRoute(from, to, route);
                log.debug(retval.toString());
                return retval;
            }
        }
    }

    public Queue<ASearchRoute> findRoutes(Cell from, Scorer<Connection> costumeScorer,
            Supplier<Collection<Cell>> routesFunc) {
        Queue<ASearchRoute> retval = new PriorityQueue<>();
        for (Cell goTo : routesFunc.get()) {
            ASearchRoute route = findRoute(from, goTo, costumeScorer);
            if (route != null) {
                retval.add(route);
            }
        }
        return retval;
    }

    public Queue<ASearchRoute> findRoutes(Cell from, Supplier<Collection<Cell>> routesFunc) {
        return findRoutes(from, null, routesFunc);
    }
}
