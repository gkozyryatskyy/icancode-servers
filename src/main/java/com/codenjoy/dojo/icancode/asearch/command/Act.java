package com.codenjoy.dojo.icancode.asearch.command;

import java.util.Set;

import com.codenjoy.dojo.icancode.asearch.model.Cell;
import com.codenjoy.dojo.icancode.client.Command;
import com.codenjoy.dojo.icancode.model.Elements;
import com.codenjoy.dojo.services.Direction;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Act implements ICommand {

    LEFT(-1, 0, Command.fire(Direction.LEFT), 0), //
    RIGHT(1, 0, Command.fire(Direction.RIGHT), 0), //
    UP(0, 1, Command.fire(Direction.UP), 0), //
    DOWN(0, -1, Command.fire(Direction.DOWN), 0);

    private final int x;
    private final int y;
    private final Command command;
    private final double score;

    /**
     * @param cell Cell from where to shot
     * @param distance how far to calculate
     * @param unstoppable do not check isCanShotThrough
     * @param targets possible targets for a shot
     * @return distance to any target. -1 means there is not target found
     */
    public int calculateShot(Cell cell, int distance, boolean unstoppable, Set<Elements> targets) {
        Cell next = cell;
        for (int i = 0; i < distance; i++) {
            if (next != null && next.isCanShotThrough(unstoppable)) {
                if (next.getE().stream().anyMatch(targets::contains)) {
                    return i;
                } else {
                    next = forceMove(next);
                }
            } else {
                break;
            }
        }
        return -1;
    }

    @Override
    public boolean isJump() {
        return false;
    }

    @Override
    @SuppressWarnings("DuplicatedCode")
    public ICommand rotated() {
        switch (this) {
            case LEFT:
                return UP;
            case UP:
                return RIGHT;
            case RIGHT:
                return DOWN;
            case DOWN:
                return LEFT;
        }
        throw new IllegalArgumentException("Cant rotate.");
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + "." + name();
    }
}
