package com.codenjoy.dojo.icancode.asearch.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.codenjoy.dojo.icancode.asearch.command.ICommand;
import com.codenjoy.dojo.icancode.asearch.command.Move;
import com.codenjoy.dojo.icancode.memory.Memory;
import com.codenjoy.dojo.icancode.model.Elements;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@AllArgsConstructor
@EqualsAndHashCode(of = { "id" })
@ToString(includeFieldNames = false, of = { "id", "e" })
public class Cell implements Comparable<Cell> {

    private final int x;
    private final int y;
    private final List<Elements> e;
    private final Memory memory;
    private final String id;

    private Long score;

    public Cell(int x, int y, Memory memory, List<Elements> layeredElements) {
        this.x = x;
        this.y = y;
        this.e = layeredElements;
        this.memory = memory;
        this.id = x + ":" + y;
    }

    public boolean is(Elements element) {
        return e.contains(element);
    }

    /**
     * @param c action from which we are accessing this cell
     *
     * @return true if we can go/jump to this cell
     */
    public boolean isCanGoThrough(Move c) {
        return e.stream().allMatch(e -> e.getCanGoThroughFunc().test(c));
    }

    public boolean isCanJumpThrough() {
        return e.stream().allMatch(Elements::isCanJumpThrough);
    }

    public boolean isCanShotThrough(boolean unstoppable) {
        return e.stream().allMatch(e -> e.getCanShotThroughFunc().test(unstoppable));
    }

    /**
     * Try to score destination cells on amount of ways out for destination
     *
     * @return count of ways out
     */
    public Long getScore() {
        if (score == null) {
            this.score = (long) new Connection(this).getConnections().size();
        }
        return score;
    }

    private static void addIfExists(Collection<Cell> coll, Cell cell) {
        if (cell != null) {
            coll.add(cell);
        }
    }

    // @formatter:off
    /**
     * getDirection(2) =
     * ..x..
     * ..x..
     * xx☺xx
     * ..x..
     * ..x..
     *
     * @param distance distance for directions calculation
     *
     * @return Set of directions cells
     */
    // @formatter:on
    public Set<Cell> getDirection(int distance) {
        Set<Cell> retval = new HashSet<>();
        // order from far to near
        for (int i = distance; i > 0; i--) {
            addIfExists(retval, memory.get(x + i, y));
            addIfExists(retval, memory.get(x - i, y));
            addIfExists(retval, memory.get(x, y + i));
            addIfExists(retval, memory.get(x, y - i));
        }
        return retval;
    }

    // @formatter:off
    /**
     * getDirectionInRadius(2) =
     * ..x..
     * .....
     * x.☺.x
     * .....
     * ..x..
     *
     * @param distance distance for direction in radius calculation
     *
     * @return Set of direction in radius cells
     */
    // @formatter:on
    public Set<Cell> getDirectionInRadius(int distance) {
        Set<Cell> retval = new HashSet<>();
        addIfExists(retval, memory.get(x + distance, y));
        addIfExists(retval, memory.get(x - distance, y));
        addIfExists(retval, memory.get(x, y + distance));
        addIfExists(retval, memory.get(x, y - distance));
        return retval;
    }

    // @formatter:off
    /**
     * getSquare(2) =
     *
     * xxxxx
     * xxxxx
     * xx☺xx
     * xxxxx
     * xxxxx
     *
     * @param radius radius for square calculation
     *
     * @return Set of square in radius cells
     */
    // @formatter:on
    public Set<Cell> getSquare(int radius) {
        Set<Cell> retval = new HashSet<>();
        // order from far to near
        for (int i = x - radius; i <= x + radius; i++) {
            for (int j = y - radius; j <= y + radius; j++) {
                if (x != i || y != j) { // exclude me
                    addIfExists(retval, memory.get(i, j));
                }
            }
        }
        return retval;
    }

    /**
     * Calculate in which corner of the map, this cell is
     *
     * @return map corner. Ex: [LEFT, DOWN] or [RIGHT, UP], etc.
     */
    public List<Move> getMapCorner() {
        List<Move> retval = new ArrayList<>();
        int middle = memory.getSize() / 2;
        if (x < middle) {
            retval.add(Move.LEFT);
        } else {
            retval.add(Move.RIGHT);
        }
        if (y < middle) {
            retval.add(Move.DOWN);
        } else {
            retval.add(Move.UP);
        }
        return retval;
    }

    /**
     * Check is this cell is a danger cell?
     *
     * @param command from which command we are accessing this cell
     *
     * @return true is this is a danger cell
     */
    public boolean isDanger(ICommand command) {
        // check if zombie is near
        if (memory.getNext1Zombies().contains(this) ||
                // check if other player is near
                memory.getNext1OtherRoboLasers().contains(this)) {
            return true;
        } else {
            // if command == null we are checking like this is not a jump, but simple move
            if (command != null && command.isJump()) {
                // if this is jump, check that we will not jump to the laser
                return memory.getNext2Lasers().contains(this);
            } else {
                // check that we will not go to the laser
                return memory.getNext1Lasers().contains(this);
            }
        }
    }

    /**
     * Used in destination cells ordering in strategies
     *
     * @param o the object to be compared.
     *
     * @return a negative integer, zero, or a positive integer as this object is less than, equal to, or greater than
     * the specified object.
     */
    @Override
    public int compareTo(Cell o) {
        int retval = getScore().compareTo(o.getScore());
        if (retval == 0) {
            return id.compareTo(o.id);
        } else {
            // using '-' for reverse order
            return -retval;
        }
    }
}
