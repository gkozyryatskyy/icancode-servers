package com.codenjoy.dojo.icancode.asearch.pattern;

import java.util.List;
import java.util.function.Predicate;

import com.codenjoy.dojo.icancode.asearch.command.ICommand;
import com.codenjoy.dojo.icancode.asearch.command.Jump;
import com.codenjoy.dojo.icancode.asearch.command.Pull;
import com.codenjoy.dojo.icancode.asearch.command.Special;
import com.codenjoy.dojo.icancode.asearch.model.Cell;
import com.codenjoy.dojo.icancode.model.Elements;

import lombok.Getter;

@Getter
public class BoxAndBlockPattern extends Pattern {

    // @formatter:off
    /**
     * pattern
     * . - any
     * c - cant go to
     * x - one of united, cant jump through
     * g - can go through
     * B - box
     *
     *   |---|
     * 4 |x.x|
     * 3 |xcx|
     * 2 |xBx|
     * 1 |xgx|
     * 0 |xgx|
     *   |---|
     *    012
     */
    // @formatter:on

    private static final CellPredicate[][] cellPattern = new CellPredicate[][] {
            { (c) -> true, (c) -> true, (c) -> true, (c) -> true, (c) -> true }, // x0
            { (c) -> c != null && c.isCanGoThrough(null) && !c.isDanger(null), // x1
                    (c) -> c != null && c.isCanGoThrough(null), //
                    (c) -> c != null && c.is(Elements.BOX), //
                    (c) -> c != null && (!c.isCanGoThrough(null) || c.is(Elements.EXIT)), //
                    (c) -> true }, //
            { (c) -> true, (c) -> true, (c) -> true, (c) -> true, (c) -> true } }; // x2

    private static final Predicate<Cell[][]> fieldPattern = (c) -> {
        // should be at lease 1 wall on each side to not match 2 boxes in the middle of the "can go" field
        return PatternUtils.isBlocked(c[0][0], c[0][1], c[0][2], c[0][3], c[0][4])
                //
                && PatternUtils.isBlocked(c[2][0], c[2][1], c[2][2], c[2][3], c[2][4]);
    };

    private static final List<ICommand> commands = List.of(Pull.DOWN, Jump.UP, Special.DO_NOTHING);

    public BoxAndBlockPattern() {
        super(new XY(1, 1), new XY(1, 2), cellPattern, fieldPattern, commands);
    }

}
