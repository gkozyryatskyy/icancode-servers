package com.codenjoy.dojo.icancode.asearch.command;

import java.util.function.Supplier;

import com.codenjoy.dojo.icancode.asearch.model.Cell;
import com.codenjoy.dojo.icancode.client.Command;
import com.codenjoy.dojo.services.Direction;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Move implements ICommand {

    LEFT(-1, 0, () -> Pull.LEFT, Command.go(Direction.LEFT), 0), //
    RIGHT(1, 0,() ->  Pull.RIGHT, Command.go(Direction.RIGHT), 0), //
    UP(0, 1, () -> Pull.UP, Command.go(Direction.UP), 0), //
    DOWN(0, -1,() ->  Pull.DOWN, Command.go(Direction.DOWN), 0);

    private final int x;
    private final int y;
    private final Supplier<Pull> pull; // using pull Supplier for not link from Move to Pull and from Pull to Move
    private final Command command;
    private final double score;

    /**
     * @param cell Cell to move
     *
     * @return moved cell. null if mem.get() will return null or failed isCanGoThrough/isCanJumpThrough checks
     */
    public Cell move(Cell cell) {
        Cell retval = forceMove(cell);
        // do not return if we cant go there
        if (retval != null && retval.isCanGoThrough(this)) {
            return retval;
        } else {
            return null;
        }
    }

    @Override
    public boolean isJump() {
        return false;
    }

    @Override
    @SuppressWarnings("DuplicatedCode")
    public ICommand rotated() {
        switch (this) {
            case LEFT:
                return UP;
            case UP:
                return RIGHT;
            case RIGHT:
                return DOWN;
            case DOWN:
                return LEFT;
        }
        throw new IllegalArgumentException("Cant rotate.");
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + "." + name();
    }
}
