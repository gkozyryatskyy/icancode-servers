package com.codenjoy.dojo.icancode.asearch.command;

import java.util.Objects;

import com.codenjoy.dojo.icancode.asearch.model.Cell;
import com.codenjoy.dojo.icancode.client.Command;
import com.codenjoy.dojo.icancode.model.Elements;
import com.codenjoy.dojo.services.Direction;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Pull implements ICommand {

    LEFT(-1, 0, Move.LEFT, Move.RIGHT, Command.pull(Direction.LEFT), 0), //
    RIGHT(1, 0, Move.RIGHT, Move.LEFT, Command.pull(Direction.RIGHT), 0), //
    UP(0, 1, Move.UP, Move.DOWN, Command.pull(Direction.UP), 0), //
    DOWN(0, -1, Move.DOWN, Move.UP, Command.pull(Direction.DOWN), 0);

    private final int x;
    private final int y;
    private final Move moveCommand;
    private final Move oppositeMoveCommand;
    private final Command command;
    private final double score;

    public Cell pull(Cell cell) {
        Cell retval = null;
        // do not use pull if in danger, because seems like laser tig goes before pull ticks
        if (!cell.isDanger(this)) {
            // check push
            Cell pushCell = forceMove(cell);
            if (pushCell != null && pushCell.is(Elements.BOX) && !pushCell.is(Elements.HOLE)) {
                if (isShouldMove(pushCell)) {
                    Cell nextPushCell = forceMove(pushCell);
                    if (nextPushCell != null
                            // can push to halls
                            && (nextPushCell.isCanGoThrough(moveCommand) || nextPushCell.is(Elements.HOLE))
                            // cant push to EXIT, START or GOLD
                            && !nextPushCell.is(Elements.EXIT) && !nextPushCell.is(Elements.START) && !nextPushCell.is(
                            Elements.GOLD)) {
                        retval = pushCell;
                    }
                }
            }
            // check pull
            Cell pullCell = oppositeMoveCommand.forceMove(cell);
            if (pullCell != null && pullCell.is(Elements.BOX)) {
                // this logic is needed to not break blocks
                if (isShouldMove(pullCell)) {
                    Cell goTo = forceMove(cell);
                    if (goTo != null && goTo.isCanGoThrough(moveCommand)) {
                        retval = goTo;
                    }
                }
            }
        }
        return retval;
    }

    boolean isShouldMove(Cell box) {
        return box.getSquare(1).stream().filter(Objects::nonNull).filter(e -> !e.isCanGoThrough(null)).count() < 4;
    }

    @Override
    public boolean isJump() {
        return false;
    }

    @Override
    @SuppressWarnings("DuplicatedCode")
    public ICommand rotated() {
        switch (this) {
            case LEFT:
                return UP;
            case UP:
                return RIGHT;
            case RIGHT:
                return DOWN;
            case DOWN:
                return LEFT;
        }
        throw new IllegalArgumentException("Cant rotate.");
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + "." + name();
    }
}
