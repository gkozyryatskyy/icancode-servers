package com.codenjoy.dojo.icancode.asearch.pattern;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@EqualsAndHashCode
@AllArgsConstructor
@ToString(includeFieldNames = false)
public class XY {

    public final int x;
    public final int y;
}
