package com.codenjoy.dojo.icancode.asearch.pattern;

import com.codenjoy.dojo.icancode.asearch.model.Cell;

public class PatternUtils {

    // you cant just through this cells

    /**
     * Check if we can jump through this cell sequence?
     *
     * @param cells cells to check. Cells should be an ordered sequence of cell (going one after another)
     *
     * @return true if we cant go or jump through input cells sequence
     */
    public static boolean isBlocked(Cell... cells) {
        Cell previous = null;
        for (Cell cell : cells) {
            if (cell == null) {
                // if cell is null, it should be out of the map, so it should be blocked
                return true;
            } else if (!cell.isCanJumpThrough()) {
                // !isCanJumpThrough indicates walls, if we cant jump through at least one, we cant jump through all of them
                return true;
            } else if (previous != null) {
                if (Math.abs(previous.getX() - cell.getX() + previous.getY() - cell.getY()) != 1) {
                    throw new IllegalArgumentException("Cells are not ordered sequence of cell");
                }
                // if we are already here, previous and cell should be isCanJumpThrough
                // so we can check if they both are !isCanGoThrough, and they 2 in a row will be a block
                if (!previous.isCanGoThrough(null) && !cell.isCanGoThrough(null)) {
                    return true;
                }
            }
            previous = cell;
        }
        return false;
    }
}
