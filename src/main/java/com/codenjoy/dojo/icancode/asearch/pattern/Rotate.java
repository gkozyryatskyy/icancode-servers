package com.codenjoy.dojo.icancode.asearch.pattern;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import com.codenjoy.dojo.icancode.asearch.command.ICommand;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.experimental.Accessors;

@Getter
@AllArgsConstructor
@Accessors(fluent = true)
@SuppressWarnings("SuspiciousNameCombination")
public enum Rotate {

    ROTATE_90((e) -> new XY(e.y, -e.x), c -> rotate(c, 0)), //
    ROTATE_180((e) -> new XY(-e.x, -e.y), c -> rotate(c, 1)), //
    ROTATE_270((e) -> new XY(-e.y, e.x), c -> rotate(c, 2));

    private final Function<XY, XY> rotateFunc;
    private final Function<List<ICommand>, List<ICommand>> rotateCommandsFunc;

    public static List<ICommand> rotate(List<ICommand> commands, int additionalRotate) {
        return commands.stream().map(c -> {
            ICommand rotated = c.rotated();
            for (int i = 0; i < additionalRotate; i++) {
                rotated = rotated.rotated();
            }
            return rotated;
        }).collect(Collectors.toList());
    }

}
