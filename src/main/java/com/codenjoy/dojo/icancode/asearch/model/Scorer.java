package com.codenjoy.dojo.icancode.asearch.model;

@FunctionalInterface
public interface Scorer <T extends GraphNode<T>> {

    double computeCost(T from, T to);
}
