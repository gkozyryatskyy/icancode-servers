package com.codenjoy.dojo.icancode.asearch.pattern;

import java.util.List;
import java.util.function.Predicate;

import com.codenjoy.dojo.icancode.asearch.command.ICommand;
import com.codenjoy.dojo.icancode.asearch.command.Jump;
import com.codenjoy.dojo.icancode.asearch.command.Pull;
import com.codenjoy.dojo.icancode.asearch.command.Special;
import com.codenjoy.dojo.icancode.asearch.model.Cell;
import com.codenjoy.dojo.icancode.model.Elements;

public class TwoJumpsInARowPattern extends Pattern {

    // @formatter:off
    /**
     * pattern
     * . - any
     * x - one of united, cant jump through
     * g - can go through
     * B - box
     * O - cant go, but can jump
     *
     *   |---|
     * 4 |.g.|
     * 3 |xBx|
     * 2 |xgx|
     * 1 |xOx|
     * 0 |xgx|
     *   |---|
     *    012
     */
    // @formatter:on

    private static final CellPredicate[][] cellPattern = new CellPredicate[][] {
            { (c) -> true, (c) -> true, (c) -> true, (c) -> true, (c) -> true }, // x0
            { (c) -> c != null && c.isCanGoThrough(null), // 0
                    (c) -> c != null && !c.isCanGoThrough(null) && c.isCanJumpThrough(), // 1
                    (c) -> c != null && c.isCanGoThrough(null) && !c.isDanger(Jump.JUMP), // 2
                    (c) -> c != null && c.is(Elements.BOX), // 3
                    (c) -> c != null && c.isCanGoThrough(null) && !c.isDanger(null) }, // 4
            { (c) -> true, (c) -> true, (c) -> true, (c) -> true, (c) -> true } }; //x2

    private static final Predicate<Cell[][]> fieldPattern = (c) -> {
        // should be at lease 1 wall on each side to not match 2 boxes in the middle of the "can go" field
        return PatternUtils.isBlocked(c[0][0], c[0][1], c[0][2], c[0][3])
                //
                && PatternUtils.isBlocked(c[2][0], c[2][1], c[2][2], c[2][3]);
    };

    private static final List<ICommand> commands = List.of(Jump.UP, Special.DO_NOTHING, Jump.UP, Special.DO_NOTHING,
            Pull.DOWN);

    public TwoJumpsInARowPattern() {
        // distance score = 3
        // commands score = 2
        // for decrease this pattern scoring, scoreAdd = -4 for [3 + 2 - 5 = 1]
        super(new XY(1, 0), new XY(1, 3), cellPattern, fieldPattern, commands, -5d);
    }
}
