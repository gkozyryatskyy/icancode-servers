package com.codenjoy.dojo.icancode.asearch.model;

import java.util.Set;

public interface GraphNode<T> {

    /**
     * Defines unique id of the destination
     * @return destination id
     */
    String getId();

    Set<T> getConnections();

}
