package com.codenjoy.dojo.icancode.asearch.model;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import com.codenjoy.dojo.icancode.asearch.command.ICommand;
import com.codenjoy.dojo.icancode.asearch.command.Jump;
import com.codenjoy.dojo.icancode.asearch.command.Move;
import com.codenjoy.dojo.icancode.asearch.command.Pull;
import com.codenjoy.dojo.icancode.model.Elements;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@EqualsAndHashCode(exclude = { "scoreAdd" })
@ToString(includeFieldNames = false)
public class Connection implements GraphNode<Connection> {

    public Cell cell; // destination cell. Can not be null
    public List<ICommand> commands; // commands, that leads to this cell. Can be null
    public double scoreAdd = 0;

    public Connection(Cell cell, List<ICommand> commands, Double scoreAdd) {
        Objects.requireNonNull(cell);
        this.cell = cell;
        this.commands = commands;
        if (scoreAdd != null) {
            this.scoreAdd = scoreAdd;
        }
    }

    protected Connection(Cell cell, ICommand command) {
        this(cell, List.of(command), null);
    }

    public Connection(Cell cell, List<ICommand> commands) {
        this(cell, commands, null);
    }

    public Connection(Cell cell) {
        this(cell, List.of(), null);
    }

    public double getScore() {
        double retval = commands.stream().mapToDouble(ICommand::getScore).sum();
        if (cell.is(Elements.EXIT) || cell.is(Elements.ZOMBIE_START)) {
            retval += 4;
        }
        return retval + scoreAdd;
    }

    @Override
    public String getId() {
        return cell.getId();
    }

    @Override
    public Set<Connection> getConnections() {
        Set<Connection> retval = new HashSet<>();
        // iterate all the possible pull commands
        Set<Pull> usedPulls = new HashSet<>();
        Arrays.stream(Pull.values()).forEach(c -> {
            Cell pull = c.pull(cell);
            if (pull != null) {
                if (shouldMoveTo(c, pull)) {
                    usedPulls.add(c);
                    retval.add(new Connection(pull, c));
                }
            }
        });
        // iterate all the possible move commands
        Arrays.stream(Move.values()).forEach(c -> {
            // if this move was already used as a pull, do not use it again
            if (!usedPulls.contains(c.getPull().get())) {
                Cell moved = c.move(cell);
                if (moved != null) {
                    if (shouldMoveTo(c, moved)) {
                        retval.add(new Connection(moved, c));
                    }
                }
            }
        });
        // iterate all the possible jump commands
        Arrays.stream(Jump.values()).forEach(c -> {
            Cell moved = c.move(cell);
            if (moved != null) {
                if (shouldMoveTo(c, moved)) {
                    retval.add(new Connection(moved, c));
                }
            }
        });
        Connection pattern = cell.getMemory().getPattern(cell);
        if (pattern != null) {
            retval.add(pattern);
        }
        return retval;
    }

    private boolean shouldMoveTo(ICommand c, Cell moved) {
        // if cell == me, then this is a first step
        if (cell.equals(cell.getMemory().getMe())) {
            return !moved.isDanger(c);
        } else {
            return true;
        }
    }
}
