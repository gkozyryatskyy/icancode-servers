package com.codenjoy.dojo.icancode.asearch.model;

public class ConnectionScorer implements Scorer<Connection> {

    @Override
    public double computeCost(Connection from, Connection to) {
        double retval = Math.abs(from.getCell().getX() - to.getCell().getX()) + Math.abs(
                from.getCell().getY() - to.getCell().getY());
        // add commands score
        retval += to.getScore();
        return retval;
    }
}
