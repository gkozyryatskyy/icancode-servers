package com.codenjoy.dojo.icancode.asearch.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.stream.Collectors;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@AllArgsConstructor
public class RouteFinder<T extends GraphNode<T>> {

    private final Graph<T> graph;
    private final Scorer<T> nextNodeScorer;
    private final Scorer<T> targetScorer;

    /**
     * Finding most optimal route
     *
     * @param from route from
     * @param to route to
     * @param costumeScorer customise next node score
     *
     * @return route
     */
    public Route<T> findRoute(T from, T to, Scorer<T> costumeScorer) {
        Map<T, RouteNode<T>> allNodes = new HashMap<>();
        Queue<RouteNode<T>> openSet = new PriorityQueue<>();

        RouteNode<T> start = new RouteNode<>(from, null, 0d, targetScorer.computeCost(from, to));
        allNodes.put(from, start);
        openSet.add(start);

        while (!openSet.isEmpty()) {
            log.trace("Open Set contains: {}", openSet.stream().map(RouteNode::getCurrent).collect(Collectors.toSet()));
            RouteNode<T> next = openSet.poll();
            if (next != null) {
                log.trace("Looking at node: {}", next);
                // checking that we are on the same cell
                if (next.getCurrent().getId().equals(to.getId())) {
                    List<T> route = new ArrayList<>();
                    RouteNode<T> current = next;
                    do {
                        route.add(0, current.getCurrent());
                        current = allNodes.get(current.getPrevious());
                    } while (current != null);
                    return new Route<>(route, next.getRouteScore());
                }

                graph.getConnections(next.getCurrent()).forEach(connection -> {
                    double newScore = next.getRouteScore() + nextNodeScorer.computeCost(next.getCurrent(), connection);
                    if (costumeScorer != null) {
                        newScore += costumeScorer.computeCost(next.getCurrent(), connection);
                    }
                    RouteNode<T> nextNode = allNodes.getOrDefault(connection, new RouteNode<>(connection));
                    allNodes.put(connection, nextNode);

                    if (nextNode.getRouteScore() > newScore) {
                        nextNode.setPrevious(next.getCurrent());
                        nextNode.setRouteScore(newScore);
                        nextNode.setEstimatedScore(newScore + targetScorer.computeCost(connection, to));
                        openSet.add(nextNode);
                    }
                });
            }
        }
        return null;
    }

    @Getter
    @AllArgsConstructor
    public static class Route<T extends GraphNode<T>> {
        private final List<T> route;
        private final double routeScore;
    }
}
