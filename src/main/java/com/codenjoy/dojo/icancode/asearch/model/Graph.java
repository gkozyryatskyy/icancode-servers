package com.codenjoy.dojo.icancode.asearch.model;

import java.util.Set;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class Graph<T extends GraphNode<T>> {

    public Set<T> getConnections(T node) {
        return node.getConnections();
    }
}
