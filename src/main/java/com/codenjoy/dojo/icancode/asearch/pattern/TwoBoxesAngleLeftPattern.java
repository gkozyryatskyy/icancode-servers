package com.codenjoy.dojo.icancode.asearch.pattern;

import java.util.List;
import java.util.function.Predicate;

import com.codenjoy.dojo.icancode.asearch.command.ICommand;
import com.codenjoy.dojo.icancode.asearch.command.Jump;
import com.codenjoy.dojo.icancode.asearch.command.Pull;
import com.codenjoy.dojo.icancode.asearch.command.Special;
import com.codenjoy.dojo.icancode.asearch.model.Cell;
import com.codenjoy.dojo.icancode.model.Elements;

public class TwoBoxesAngleLeftPattern extends Pattern {

    // @formatter:off
    /**
     * pattern
     * . - any
     * x - one of united, cant jump through
     * g - can go through
     * B - box
     *
     *   |---|
     * 2 |gxx|
     * 1 |gBg|
     * 0 |Bxx|
     *   |---|
     *    012
     */


    private static final CellPredicate[][] cellPattern = new CellPredicate[][] {
            { (c) -> c != null && c.is(Elements.BOX),  // x1
              (c) -> c != null && c.isCanGoThrough(null) && !c.isDanger(null),
              (c) -> c != null && c.isCanGoThrough(null) && !c.isDanger(null) },
            { (c) -> true, (c) -> c != null && c.is(Elements.BOX), (c) -> true }, // x2
            { (c) -> true, (c) -> c != null && c.isCanGoThrough(null), (c) -> true} }; //x3
    // @formatter:on

    private static final Predicate<Cell[][]> fieldPattern = (c) -> {
        // should be at lease 1 wall on each side to not match 2 boxes in the middle of the "can go" field
        return PatternUtils.isBlocked(c[1][0], c[2][0])
                //
                && PatternUtils.isBlocked(c[1][2], c[2][2]);
    };

    private static final List<ICommand> commands = List.of(Jump.LEFT, Special.DO_NOTHING, Pull.UP);

    public TwoBoxesAngleLeftPattern() {
        super(new XY(2, 1), new XY(0, 2), cellPattern, fieldPattern, commands, -4d);
    }
}
