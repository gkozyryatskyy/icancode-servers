package com.codenjoy.dojo.icancode.asearch.command;

import com.codenjoy.dojo.icancode.client.Command;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Special implements ICommand {

    DO_NOTHING(0,0, Command.doNothing(), 0), DIE(0,0, Command.die(), 0);

    private final int x;
    private final int y;
    private final Command command;
    private final double score;

    @Override
    public boolean isJump() {
        return false;
    }

    @Override
    public ICommand rotated() {
        return this;
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + "." + name();
    }
}
