package com.codenjoy.dojo.icancode.asearch;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import com.codenjoy.dojo.icancode.asearch.command.ICommand;
import com.codenjoy.dojo.icancode.asearch.model.Cell;
import com.codenjoy.dojo.icancode.asearch.model.Connection;
import com.codenjoy.dojo.icancode.asearch.model.RouteFinder;

import lombok.Getter;
import lombok.ToString;

@ToString(exclude = "commands") // excluding commands for not init them if no need
public class ASearchRoute implements Comparable<ASearchRoute> {

    @Getter
    private final Cell from;
    @Getter
    private final Cell to;
    @Getter
    private final List<Cell> route;
    private final List<List<ICommand>> commands;
    private final double routeCost;

    private Double finalCost;

    public Double getFinalCost() {
        if (finalCost == null) {
            // TODO you can extend finalCost here according do destination (to) surrounding
            finalCost = routeCost;
        }
        return finalCost;
    }

    public ASearchRoute(Cell from, Cell to, RouteFinder.Route<Connection> findRoute) {
        this.from = from;
        this.to = to;
        this.route = new ArrayList<>();
        this.commands = new ArrayList<>();
        findRoute.getRoute().forEach(e -> {
            route.add(e.getCell());
            commands.add(e.getCommands());
        });
        this.routeCost = findRoute.getRouteScore();
    }

    public List<ICommand> getNextCommands() {
        if (commands != null) {
            for (List<ICommand> next : commands) {
                if (!next.isEmpty()) {
                    return next;
                }
            }
        }
        return null;
    }

    // for tests
    public ICommand getNextCommand() {
        List<ICommand> c = getNextCommands();
        return c != null ? c.get(0) : null;
    }

    // for tests
    public List<ICommand> getFlatCommands() {
        if (commands != null) {
            return commands.stream().flatMap(Collection::stream).collect(Collectors.toList());
        } else {
            return null;
        }
    }

    /**
     * @return route size. Amount of ticks to the finish
     */
    public int size() {
        if (commands != null) {
            return commands.stream()
                    .flatMap(Collection::stream)
                    .filter(Objects::nonNull)
                    // jump is 2 ticks, others 1 tick
                    .mapToInt(e -> e.isJump() ? 2 : 1)
                    .sum();
        } else {
            return 0;
        }
    }

    @Override
    public int compareTo(ASearchRoute other) {
        return Double.compare(getFinalCost(), other.getFinalCost());
    }
}
