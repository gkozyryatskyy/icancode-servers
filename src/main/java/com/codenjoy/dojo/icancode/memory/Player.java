package com.codenjoy.dojo.icancode.memory;

import com.codenjoy.dojo.icancode.Main;
import com.codenjoy.dojo.icancode.asearch.model.Cell;
import com.codenjoy.dojo.icancode.model.Elements;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString(onlyExplicitlyIncluded = true)
public class Player {

    @ToString.Include
    private Cell cell;
    private Memory currentTurn;
    @ToString.Include
    private boolean alive;

    @ToString.Include(name = "rec")
    private int recharge;
    @ToString.Include(name = "g")
    private int gold;
    @ToString.Include(name = "uL")
    private int unstoppableLaser;
    @ToString.Include(name = "dR")
    private int deathRay;
    @ToString.Include(name = "uF")
    private int unlimitedFire;

    public Player(Memory currentTurn, Cell player) {
        this.cell = player;
        this.currentTurn = currentTurn;
        tick(null, currentTurn, player);
    }

    public void tick(Memory previousTurn, Memory currentTurn, Cell player) {
        this.cell = player;
        this.currentTurn = currentTurn;
        this.alive = cell.is(Elements.ROBO) || cell.is(Elements.ROBO_FLYING) || cell.is(Elements.ROBO_OTHER) || cell.is(
                Elements.ROBO_OTHER_FLYING);
        if (this.recharge > 0) {
            this.recharge--;
        }
        if (this.unstoppableLaser > 0) {
            this.unstoppableLaser--;
        }
        if (this.deathRay > 0) {
            this.deathRay--;
        }
        if (this.unlimitedFire > 0) {
            this.unlimitedFire--;
        }
        if (previousTurn != null) {
            Cell prevCell = previousTurn.get(player.getX(), player.getY());
            // jump through gold or perks will not take them, so check !isFlying()
            if (prevCell != null && !isFlying()) {
                if (prevCell.is(Elements.GOLD)) {
                    this.gold++;
                } else if (prevCell.is(Elements.UNSTOPPABLE_LASER_PERK)) {
                    this.unstoppableLaser = Main.PERKS_TIME;
                } else if (prevCell.is(Elements.DEATH_RAY_PERK)) {
                    this.deathRay = Main.PERKS_TIME;
                } else if (prevCell.is(Elements.UNLIMITED_FIRE_PERK)) {
                    this.unlimitedFire = Main.PERKS_TIME;
                }
            }
        }
        // jump through exit can clean the counters, so check !isFlying()
        if (!alive || (player.is(Elements.EXIT) && !isFlying())) {
            this.recharge = 0;
            this.gold = 0;
            this.unstoppableLaser = 0;
            this.deathRay = 0;
            this.unlimitedFire = 0;
        }
    }

    public void shot() {
        if (unlimitedFire <= 0) {
            this.recharge = Main.SHOT_RECHARGE;
        }
    }

    public boolean isFlying() {
        return cell.is(Elements.ROBO_FLYING) || cell.is(Elements.ROBO_OTHER_FLYING);
    }

}
