package com.codenjoy.dojo.icancode.memory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import com.codenjoy.dojo.icancode.asearch.command.Move;
import com.codenjoy.dojo.icancode.asearch.model.Cell;
import com.codenjoy.dojo.icancode.asearch.model.Connection;
import com.codenjoy.dojo.icancode.asearch.pattern.BoxAndBlockPattern;
import com.codenjoy.dojo.icancode.asearch.pattern.BoxInAngleLeftPattern;
import com.codenjoy.dojo.icancode.asearch.pattern.BoxInAngleRightPattern;
import com.codenjoy.dojo.icancode.asearch.pattern.Pattern;
import com.codenjoy.dojo.icancode.asearch.pattern.TwoBoxesAngleRightPattern;
import com.codenjoy.dojo.icancode.asearch.pattern.TwoBoxesInAnglePattern;
import com.codenjoy.dojo.icancode.asearch.pattern.TwoBoxesAngleLeftPattern;
import com.codenjoy.dojo.icancode.asearch.pattern.TwoBoxesPattern;
import com.codenjoy.dojo.icancode.asearch.pattern.TwoJumpsInARowPattern;
import com.codenjoy.dojo.icancode.asearch.pattern.XY;
import com.codenjoy.dojo.icancode.client.ExtendedBoard;
import com.codenjoy.dojo.icancode.model.ElementGroup;
import com.codenjoy.dojo.icancode.model.Elements;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Memory {

    private static final List<Pattern> PATTERNS = List.of(new TwoBoxesPattern(), new TwoJumpsInARowPattern(),
            new TwoBoxesAngleLeftPattern(), new TwoBoxesAngleRightPattern(), new TwoBoxesInAnglePattern(),
            new BoxInAngleRightPattern(), new BoxInAngleLeftPattern(),
            // BoxAndBlockPattern is last, because we should check it with last priority
            new BoxAndBlockPattern());

    @Getter
    private final int size;
    @Getter
    private final int layersCount;
    private final Cell[][] cells; // current round board
    private final Map<Elements, List<Cell>> cache = new HashMap<>(); // current round elements cache
    private final Map<ElementGroup, Set<Cell>> cache1 = new HashMap<>(); // current round elements cache
    private final Map<Cell, Connection> patternsCache = new HashMap<>(); // current round patterns cache
    // support
    @Getter
    private final Set<Cell> next1Lasers = new HashSet<>();
    @Getter
    private final Set<Cell> next2Lasers = new HashSet<>();
    @Getter
    private final Set<Cell> next1Zombies;
    @Getter
    private final Set<Cell> next1OtherRoboLasers;
    @Getter
    private final LongMemory longMemory;

    public Memory(ExtendedBoard board, LongMemory longMemory) {
        this.size = board.size();
        this.layersCount = board.extGetField().length;
        this.cells = new Cell[size][size];
        for (int x = 0; x < size; x++) {
            for (int y = 0; y < size; y++) {
                // add cell
                List<Elements> e = new ArrayList<>(layersCount);
                for (int l = 0; l < layersCount; l++) {
                    e.add(board.extGetAt(l, x, y));
                }
                this.cells[x][y] = new Cell(x, y, this, e);
            }
        }
        this.longMemory = longMemory;
        // init support
        // init next turn lasers
        this.next1Lasers.addAll(
                get(Elements.ROBO_OTHER)); // do not go with a regular command to other robot, he can shot you
        this.next1Lasers.addAll(moveGroup(ElementGroup.LASERS, 1, 1));
        this.next1Lasers.addAll(moveGroup(ElementGroup.LASER_MACHINES_READY, 0, 1));
        // next2Lasers used for jumps. See {@link com.codenjoy.dojo.icancode.asearch.model.Connection#filterMove(MoveCommand, Cell)}
        this.next2Lasers.addAll(moveGroup(ElementGroup.LASERS, 1, 2));
        this.next2Lasers.addAll(moveGroup(ElementGroup.LASER_MACHINES_READY, 0, 2));
        // if other robo stop and shooting at you, and you jumping at him, you will jump on the laser
        this.next2Lasers.addAll(move(get(Elements.ROBO_OTHER), Elements.ROBO_OTHER.getLayer(), 2));
        // laser machine can become chagres on your jump and you will jump to the laser
        this.next2Lasers.addAll(moveGroup(ElementGroup.LASER_MACHINES, 0, 1));
        // init zombies
        this.next1Zombies = moveGroup(ElementGroup.ZOMBIE, 1, 1);
        // init other robots possible shots
//        this.next1OtherRoboLasers = new HashSet<>(); //TODO stub for KillerStrategy
        this.next1OtherRoboLasers = move(get(Elements.ROBO_OTHER), Elements.ROBO_OTHER.getLayer(), 1);
        // tick long memory
        longMemory.tick(this);
    }

    private Set<Cell> moveGroup(ElementGroup group, int layer, int turns) {
        return move(get(group), layer, turns);
    }

    public static Set<Cell> move(Collection<Cell> cells, int layer, int turns) {
        return cells.stream().map(e -> {
            // laser is on the 2nd layer (index 1)
            Elements el = e.getE().get(layer);
            if (el.getMovingFunc() != null) {
                return el.getMovingFunc().apply(e, turns);
            } else {
                return null;
            }
        }).filter(Objects::nonNull).flatMap(Collection::stream).collect(Collectors.toSet());
    }

    public Cell get(XY xy) {
        return get(xy.x, xy.y);
    }

    public Cell get(int x, int y) {
        if (x < 0 || y < 0 || y > size - 1 || x > size - 1) {
            // stub for coordinated out of field
            return null;
        } else {
            return cells[x][y];
        }
    }

    // keep the order of perk groups!
    public List<Cell> get(Collection<Elements> elements) {
        return elements.stream().flatMap(e -> get(e).stream()).collect(Collectors.toList());
    }

    public Set<Cell> get(Elements... elements) {
        return Arrays.stream(elements).flatMap(e -> get(e).stream()).collect(Collectors.toSet());
    }

    public List<Cell> get(Elements element) {
        List<Cell> retval = cache.get(element);
        if (retval == null) {
            retval = get0(element);
            cache.put(element, retval);
        }
        return retval;
    }

    private List<Cell> get0(Elements element) {
        return Arrays.stream(cells).flatMap(Arrays::stream).filter(e -> e.is(element)).collect(Collectors.toList());
    }

    // --------------------------------- groups ---------------------------------

    public Set<Cell> get(ElementGroup element) {
        Set<Cell> retval = cache1.get(element);
        if (retval == null) {
            retval = element.getFunc().apply(this);
            cache1.put(element, retval);
        }
        return retval;
    }

    // --------------------------------- patterns ---------------------------------

    public Connection getPattern(Cell cell) {
        Connection retval = patternsCache.get(cell);
        if (retval == null) {
            for (Pattern p : PATTERNS) {
                retval = p.match(cell);
                if (retval != null) {
                    patternsCache.put(cell, retval);
                    break;
                }
            }
            // if pattern was not found, cache a null, for not to search again, for this cell, next time
            if (retval == null) {
                patternsCache.put(cell, null);
            }
        }
        return retval;
    }

    // --------------------------------- definitions ---------------------------------

    public Set<Cell> getMapBorders() {
        Set<Cell> retval = new HashSet<>();
        int last = size - 1;
        for (int i = 0; i < size; i++) {
            retval.add(get(0, i));
            retval.add(get(last, i));
            // do not duplicate in corners
            if (i != 0 || i != last) {
                retval.add(get(i, 0));
                retval.add(get(i, last));
            }
        }
        return retval;
    }

    public Set<Cell> getMapBorder(Move side) {
        Set<Cell> retval = new HashSet<>();
        switch (side) {
            case DOWN:
                return IntStream.range(0, size).mapToObj(e -> get(e, 0)).collect(Collectors.toSet());
            case RIGHT:
                return IntStream.range(0, size).mapToObj(e -> get(size - 1, e)).collect(Collectors.toSet());
            case UP:
                return IntStream.range(0, size).mapToObj(e -> get(e, size - 1)).collect(Collectors.toSet());
            case LEFT:
                return IntStream.range(0, size).mapToObj(e -> get(0, e)).collect(Collectors.toSet());
            default:
                return Set.of();
        }
    }

    public Cell getMe() {
        return get(ElementGroup.ME).stream().findAny().orElse(null);
    }

    // --------------------------------- streams ---------------------------------

    public Stream<Cell> stream() {
        return Arrays.stream(cells).flatMap(Arrays::stream);
    }
}
