package com.codenjoy.dojo.icancode.memory;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.collections4.queue.CircularFifoQueue;

import com.codenjoy.dojo.icancode.asearch.model.Cell;
import com.codenjoy.dojo.icancode.model.ElementGroup;
import com.codenjoy.dojo.icancode.model.Elements;

import lombok.Getter;
import lombok.ToString;

@ToString
public class LongMemory {

    // using 5 size, because new round starts with 5 counts
    // first is older
    @ToString.Exclude
    private final CircularFifoQueue<Memory> memoryQueue = new CircularFifoQueue<>(5);
    @Getter
    private Player me;
    @Getter
    @Deprecated //TODO support others
    private List<Player> others;

    public void tick(Memory memory) {
        Cell meCell = memory.getMe();
        // init
        if (me == null || others == null || memoryQueue.isEmpty()) {
            // im was dead, and this is start of the round
            this.me = new Player(memory, meCell);
            this.others = memory.get(ElementGroup.OTHER_ALIVE_ROBOTS)
                    .stream()
                    .map(c -> new Player(memory, c))
                    .collect(Collectors.toList());
        } else {
            // tick of the new round
            Memory prev = memoryQueue.get(memoryQueue.size() - 1);
            // check me on the previous perk
            this.me.tick(prev, memory, meCell);
            // merger prev other players with their new cells, iteration over new cells to get current values
            //            Set<Cell> othersAlive = memory.get(ElementGroup.OTHER_ALIVE_ROBOTS);
            //            othersAlive.add(meCell); // add me because I can stand on other robot
            //            othersAlive.forEach(other -> {
            //                Set<Cell> connections = other.getDirection(1);
            //                connections.add(other); // other player not move
            //                Player prevPlayer = null;
            //                Iterator<Player> it = others.iterator();
            //                // find player
            //                while (it.hasNext()) {
            //                    prevPlayer = it.next();
            //                    if (connections.contains(prevPlayer.getCell())) {
            //                        it.remove();
            //                        break;
            //                    }
            //                }
            //                if (prevPlayer != null) {
            //                    prevPlayer.tick(prev, memory, c);
            //                }
            //            });
        }
        if (me.isAlive()) {
            // add current mem to memoryQueue
            memoryQueue.add(memory);
        } else {
            // in new round I can be moved to different map part, with different board
            // so clean everything up
            this.memoryQueue.clear();
        }
    }

    private boolean isDead(Cell me) {
        return me.is(Elements.ROBO_FALLING) || !me.is(Elements.ROBO_LASER);
    }

    private void findOtherPlayerPreviousPosition() {

    }
}
