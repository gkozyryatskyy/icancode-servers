package com.codenjoy.dojo.icancode;

import com.codenjoy.dojo.client.WebSocketRunner;
import com.codenjoy.dojo.icancode.solver.ASearchSolver;
import com.codenjoy.dojo.icancode.strategy.GoldStrategy;

public class Main {

    public static final int SHOT_RECHARGE = 3;
    public static final int PERKS_TIME = 10;
    public static final int PERKS_DEATH_RAY_LENGTH = 10;

    public static final int ZOMBIE_SPEED = 2; // i step per 2 turns

    public static final int DEATH_ON_STUCK = 5;

    public static final String player = "gst2c08w41ge6ed16rzu";
    public static final String code = "";
    public static final boolean print = false;

    public static void main(String[] args) {
        WebSocketRunner.PRINT_TO_CONSOLE = print;
        GoldStrategy strategy = new GoldStrategy();
        ASearchSolver.connectClient(
                String.format("https://epam-botchallenge.com/codenjoy-contest/board/player/%s?code=%s", player, code),
                new ASearchSolver(strategy), print);
    }
}
