package com.codenjoy.dojo.icancode.snapshot;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Snapshot {

    public static void warn(String line) {
        log.warn(line);
    }

    public static void error(String line) {
        log.error(line);
    }
}
