package com.codenjoy.dojo.icancode.asearch.pattern;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.List;
import java.util.Queue;

import org.junit.Test;

import com.codenjoy.dojo.icancode.BaseBoardTest;
import com.codenjoy.dojo.icancode.asearch.ASearch;
import com.codenjoy.dojo.icancode.asearch.ASearchRoute;
import com.codenjoy.dojo.icancode.asearch.command.ICommand;
import com.codenjoy.dojo.icancode.asearch.command.Jump;
import com.codenjoy.dojo.icancode.asearch.command.Pull;
import com.codenjoy.dojo.icancode.asearch.command.Special;
import com.codenjoy.dojo.icancode.asearch.model.Connection;
import com.codenjoy.dojo.icancode.memory.Memory;
import com.codenjoy.dojo.icancode.model.Elements;
import com.codenjoy.dojo.icancode.strategy.GoldStrategy;
import com.codenjoy.dojo.icancode.strategy.StrategyResult;

public class TwoJumpsInARowPatternTest extends BaseBoardTest {

    private final TwoJumpsInARowPattern pattern = new TwoJumpsInARowPattern();

    @Test
    public void patternTest1() {
        // @formatter:off
        Memory memory = board(
                //       0123456
                "╔═════┐" + // 6
                        "║.....│" + // 5
                        "║.....│" + // 4
                        "║.....│" + // 3
                        "║.║...│" + // 2
                        "║E....│" + // 1
                        "└─────┘", // 0
                //       0123456
                        "-------" + // 6
                        "-☺-----" + // 5
                        "-B-----" + // 4
                        "-------" + // 3
                        "-B-----" + // 2
                        "-------" + // 1
                        "-------", // 0
                //       0123456
                        "-------" + // 6
                        "-------" + // 5
                        "-------" + // 4
                        "-------" + // 3
                        "-------" + // 2
                        "-------" + // 1
                        "-------"); // 0;
        // @formatter:on
        Connection expected = new Connection(memory.get(1, 2),
                List.of(Jump.DOWN, Special.DO_NOTHING, Jump.DOWN, Special.DO_NOTHING, Pull.UP));
        assertEquals(expected, pattern.match(memory.getMe()));
        assertEquals(expected, memory.getPattern(memory.getMe()));
    }

    @Test
    public void patternTest2() {
        // @formatter:off
        Memory memory = board(
                //       0123456
                "╔═════┐" + // 6
                        "║...═.│" + // 5
                        "║....E│" + // 4
                        "║...═.│" + // 3
                        "║.....│" + // 2
                        "║.....│" + // 1
                        "└─────┘", // 0
                //       0123456
                "-------" + // 6
                        "-------" + // 5
                        "-☺B-B--" + // 4
                        "-------" + // 3
                        "-------" + // 2
                        "-------" + // 1
                        "-------", // 0
                //       0123456
                "-------" + // 6
                        "-------" + // 5
                        "-------" + // 4
                        "-------" + // 3
                        "-------" + // 2
                        "-------" + // 1
                        "-------"); // 0;
        // @formatter:on
        Connection expected = new Connection(memory.get(4, 4),
                List.of(Jump.RIGHT, Special.DO_NOTHING, Jump.RIGHT, Special.DO_NOTHING, Pull.LEFT));
        assertEquals(expected, pattern.match(memory.getMe()));
        assertEquals(expected, memory.getPattern(memory.getMe()));
    }

    @Test
    public void patternWithStrategyTest1() {
        // @formatter:off
        Memory memory = board(
                //       0123456
                "╔═════┐" + // 6
                        "║.│$..│" + // 5
                        "║.┘...│" + // 4
                        "║.....│" + // 3
                        "║.─┐..│" + // 2
                        "║..│..│" + // 1
                        "└─────┘", // 0
                //       0123456
                "-------" + // 6
                        "-------" + // 5
                        "-------" + // 4
                        "-☺B-B--" + // 3
                        "-------" + // 2
                        "-------" + // 1
                        "-------", // 0
                //       0123456
                "-------" + // 6
                        "-------" + // 5
                        "-------" + // 4
                        "-------" + // 3
                        "-------" + // 2
                        "-------" + // 1
                        "-------"); // 0;
        // @formatter:on
        // pattern test
        List<ICommand> expectedC = List.of(Jump.RIGHT, Special.DO_NOTHING, Jump.RIGHT, Special.DO_NOTHING, Pull.LEFT);
        assertEquals(new Connection(memory.get(4, 3), expectedC), pattern.match(memory.getMe()));
        // strategy test
        GoldStrategy st = new GoldStrategy();
        StrategyResult res = st.result(new ASearch(memory));
        assertEquals(expectedC, res.getCommands());
    }

    @Test
    public void patternWithStrategyTest2() {
        // @formatter:off
        Memory memory = board(
                //       0123456
                "╔═════┐" + // 6
                        "║.│$..│" + // 5
                        "║.┘...│" + // 4
                        "║.O...│" + // 3
                        "║.─┐..│" + // 2
                        "║..│..│" + // 1
                        "└─────┘", // 0
                //       0123456
                "-------" + // 6
                        "-------" + // 5
                        "-------" + // 4
                        "-☺--B--" + // 3
                        "-------" + // 2
                        "-------" + // 1
                        "-------", // 0
                //       0123456
                "-------" + // 6
                        "-------" + // 5
                        "-------" + // 4
                        "-------" + // 3
                        "-------" + // 2
                        "-------" + // 1
                        "-------"); // 0;
        // @formatter:on
        // pattern test
        List<ICommand> expectedC = List.of(Jump.RIGHT, Special.DO_NOTHING, Jump.RIGHT, Special.DO_NOTHING, Pull.LEFT);
        assertEquals(new Connection(memory.get(4, 3), expectedC), pattern.match(memory.getMe()));
        // strategy test
        GoldStrategy st = new GoldStrategy();
        StrategyResult res = st.result(new ASearch(memory));
        assertEquals(expectedC, res.getCommands());
    }

    @Test
    public void twoCellsBetweenTwoBoxes() {
        // @formatter:off
        Memory memory = board(
                //       0123456
                "╔══════┐" + // 7
                        "║......│" + // 6
                        "║......│" + // 5
                        "║......│" + // 4
                        "║......│" + // 3
                        "║.║....│" + // 2
                        "║E.....│" + // 1
                        "└──────┘", // 0
                //       01234567
                        "--------" + // 7
                        "-☺------" + // 6
                        "-B------" + // 5
                        "--------" + // 4
                        "--------" + // 3
                        "-B------" + // 2
                        "--------" + // 1
                        "--------", // 0
                //       01234567
                        "--------" + // 7
                        "--------" + // 6
                        "--------" + // 5
                        "--------" + // 4
                        "--------" + // 3
                        "--------" + // 2
                        "--------" + // 1
                        "--------"); // 0;
        // @formatter:on
        ASearch search = new ASearch(memory);
        Queue<ASearchRoute> routes = search.findRoutes(memory.getMe(), () -> memory.get(Elements.EXIT));
        assertEquals(1, routes.size());
        ASearchRoute route = routes.poll();
        assertNotNull(route);
        assertEquals(Pull.DOWN, route.getNextCommand());
        // check with moved board, that now we are using pattern
        // @formatter:off
        Memory memory2 = board(
                //       0123456
                "╔══════┐" + // 7
                        "║......│" + // 6
                        "║......│" + // 5
                        "║......│" + // 4
                        "║......│" + // 3
                        "║.║....│" + // 2
                        "║E.....│" + // 1
                        "└──────┘", // 0
                //       01234567
                        "--------" + // 7
                        "--------" + // 6
                        "-☺------" + // 5
                        "-B------" + // 4
                        "--------" + // 3
                        "-B------" + // 2
                        "--------" + // 1
                        "--------", // 0
                //       01234567
                        "--------" + // 7
                        "--------" + // 6
                        "--------" + // 5
                        "--------" + // 4
                        "--------" + // 3
                        "--------" + // 2
                        "--------" + // 1
                        "--------"); // 0;
        // @formatter:on
        search = new ASearch(memory2);
        routes = search.findRoutes(memory2.getMe(), () -> memory2.get(Elements.EXIT));
        assertEquals(1, routes.size());
        route = routes.poll();
        assertNotNull(route);
        assertEquals(List.of(Jump.DOWN, Special.DO_NOTHING, Jump.DOWN, Special.DO_NOTHING, Pull.UP),
                route.getNextCommands());
    }
}
