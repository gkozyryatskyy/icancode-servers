package com.codenjoy.dojo.icancode.asearch.command;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.codenjoy.dojo.icancode.BaseBoardTest;
import com.codenjoy.dojo.icancode.memory.Memory;

public class PullTest {

    @Test
    public void canPushOnHole() {
        Memory memory = BaseBoardTest.board(//
                "╔═══┐" + // 4
                        "║.O.│" + // 3
                        "║...│" + // 2
                        "║.S.│" + // 1
                        "└───┘", // 0
                "-----" + // 4
                        "-----" + // 3
                        "--B--" + // 2
                        "--☺--" + // 1
                        "-----", // 0
                "-----" + // 4
                        "-----" + // 3
                        "-----" + // 2
                        "-----" + // 1
                        "-----"); // 0
        assertEquals(memory.get(2,2), Pull.UP.pull(memory.getMe()));
    }
}
