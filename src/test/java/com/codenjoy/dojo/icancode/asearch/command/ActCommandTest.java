package com.codenjoy.dojo.icancode.asearch.command;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.codenjoy.dojo.icancode.BaseBoardTest;
import com.codenjoy.dojo.icancode.memory.Memory;
import com.codenjoy.dojo.icancode.strategy.GoldStrategy;
import com.codenjoy.dojo.icancode.strategy.KillerStrategy;

public class ActCommandTest {

    @Test
    public void shootZombieTest() {
        Memory memory = BaseBoardTest.board(//
                "╔═══┐" + // 4
                        "║...│" + // 3
                        "║...│" + // 2
                        "║.SE│" + // 1
                        "└───┘", // 0
                "-----" + // 4
                        "--♀--" + // 3
                        "-X-♂-" + // 2
                        "-X☺--" + // 1
                        "-----", // 0
                "-----" + // 4
                        "-----" + // 3
                        "-----" + // 2
                        "-----" + // 1
                        "-----"); // 0
        assertEquals(-1, Act.DOWN.calculateShot(memory.getMe(), 10, false, KillerStrategy.ACT_TARGETS));
        assertEquals(2, Act.UP.calculateShot(memory.getMe(), 10, false, KillerStrategy.ACT_TARGETS));
        assertEquals(1, Act.LEFT.calculateShot(memory.getMe(), 10, false, KillerStrategy.ACT_TARGETS));
        assertEquals(-1, Act.RIGHT.calculateShot(memory.getMe(), 10, false, KillerStrategy.ACT_TARGETS));
    }

    @Test
    public void unstoppableShotTest() {
        Memory memory = BaseBoardTest.board(//
                "╔═══┐" + // 4
                        "║...│" + // 3
                        "║...│" + // 2
                        "║.SE│" + // 1
                        "└───┘", // 0
                "-----" + // 4
                        "--♀--" + // 3
                        "--B--" + // 2
                        "--☺--" + // 1
                        "-----", // 0
                "-----" + // 4
                        "-----" + // 3
                        "-----" + // 2
                        "-----" + // 1
                        "-----"); // 0
        // cant shot through box
        assertEquals(-1, Act.UP.calculateShot(memory.getMe(), 10, false, KillerStrategy.ACT_TARGETS));
        // shot through box
        assertEquals(2, Act.UP.calculateShot(memory.getMe(), 10, true, KillerStrategy.ACT_TARGETS));
    }
}
