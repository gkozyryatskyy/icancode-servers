package com.codenjoy.dojo.icancode.asearch.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Test;

import com.codenjoy.dojo.icancode.BaseBoardTest;
import com.codenjoy.dojo.icancode.asearch.command.Jump;
import com.codenjoy.dojo.icancode.asearch.command.Move;
import com.codenjoy.dojo.icancode.asearch.command.Pull;
import com.codenjoy.dojo.icancode.memory.Memory;
import com.codenjoy.dojo.icancode.model.Elements;

public class CellTest extends BaseBoardTest {

    @Test
    public void isRoboTest() {
        Memory memory = board(defaultBoard);
        Cell cell = memory.get(2, 8);
        assertTrue(cell.is(Elements.ROBO));
        cell = memory.getMe();
        assertTrue(cell.is(Elements.ROBO));
    }

    @Test
    public void getConnectionsTest() {
        Memory memory = board(defaultBoard);
        Cell cell = memory.getMe();
        Set<Connection> expected = Set.of(new Connection(memory.get(2, 8), Jump.JUMP),
                new Connection(memory.get(1, 8), Move.LEFT), new Connection(memory.get(3, 8), Move.RIGHT),
                new Connection(memory.get(2, 9), Pull.UP));
        assertEquals(expected, new Connection(cell).getConnections());
    }

    @Test
    public void getDirectionTest() {
        Memory memory = board(defaultBoard);
        Cell cell = memory.getMe();
        Set<Cell> expected = new HashSet<>(
                List.of(memory.get(1, 8), memory.get(3, 8), memory.get(2, 7),
                        memory.get(2, 9)));
        assertEquals(expected, cell.getDirection(1));
        expected.addAll(List.of(memory.get(0, 8), memory.get(4, 8), memory.get(2, 6),
                memory.get(2, 10)));
        assertEquals(expected, cell.getDirection(2));
    }

    @Test
    public void getDirectionInRadiusTest() {
        Memory memory = board(defaultBoard);
        Cell cell = memory.getMe();
        Set<Cell> expected = Set.of(memory.get(1, 8), memory.get(3, 8), memory.get(2, 7),
                memory.get(2, 9));
        assertEquals(expected, cell.getDirectionInRadius(1));
        expected = Set.of(memory.get(0, 8), memory.get(4, 8), memory.get(2, 6),
                memory.get(2, 10));
        assertEquals(expected, cell.getDirectionInRadius(2));
    }

    @Test
    public void getRadiusTest() {
        Memory memory = board(defaultBoard);
        Cell cell = memory.getMe();
        Set<Cell> expected = new HashSet<>(
                List.of(memory.get(1, 7), memory.get(1, 8), memory.get(1, 9), memory.get(2, 7), memory.get(2, 9),
                        memory.get(3, 7), memory.get(3, 8), memory.get(3, 9)));
        assertEquals(expected, cell.getSquare(1));
        expected.addAll(
                List.of(memory.get(0, 6), memory.get(0, 7), memory.get(0, 8), memory.get(0, 9), memory.get(0, 10),
                        memory.get(1, 6), memory.get(2, 6), memory.get(3, 6), memory.get(1, 10), memory.get(2, 10),
                        memory.get(3, 10), memory.get(4, 6), memory.get(4, 7), memory.get(4, 8), memory.get(4, 9),
                        memory.get(4, 10)));
        assertEquals(expected, cell.getSquare(2));
    }

    @Test
    public void isDangerTest1() {
        Memory memory = BaseBoardTest.board(//
                "╔═══┐" + // 4
                        "║...│" + // 3
                        "║...│" + // 2
                        "║►SE│" + // 1
                        "└───┘", // 0
                "-----" + // 4
                        "-----" + // 3
                        "-----" + // 2
                        "--☺--" + // 1
                        "-----", // 0
                "-----" + // 4
                        "-----" + // 3
                        "-----" + // 2
                        "-----" + // 1
                        "-----"); // 0
        assertTrue(memory.getMe().isDanger(null));
    }

}
