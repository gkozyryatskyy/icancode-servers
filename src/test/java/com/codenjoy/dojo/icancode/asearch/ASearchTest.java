package com.codenjoy.dojo.icancode.asearch;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;

import com.codenjoy.dojo.icancode.BaseBoardTest;
import com.codenjoy.dojo.icancode.asearch.command.ICommand;
import com.codenjoy.dojo.icancode.asearch.command.Jump;
import com.codenjoy.dojo.icancode.asearch.command.Pull;
import com.codenjoy.dojo.icancode.asearch.model.Cell;
import com.codenjoy.dojo.icancode.asearch.command.Move;
import com.codenjoy.dojo.icancode.memory.Memory;
import com.codenjoy.dojo.icancode.model.Elements;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ASearchTest extends BaseBoardTest {

    @Test
    public void findRouteTest() {
        Memory memory = board(defaultBoard);
        ASearch search = new ASearch(memory);
        ASearchRoute route = search.findRoute(memory.getMe(), memory.get(Elements.EXIT).get(0));
        List<Cell> expectedRoute = List.of(memory.get(2, 8), memory.get(1, 8), memory.get(1, 7), memory.get(1, 6),
                memory.get(1, 5), memory.get(1, 4), memory.get(1, 3), memory.get(1, 2), memory.get(2, 2),
                memory.get(3, 2), memory.get(3, 1), memory.get(4, 1), memory.get(5, 1), memory.get(6, 1),
                memory.get(7, 1), memory.get(8, 1), memory.get(9, 1));
        assertEquals(memory.get(2, 8), route.getFrom());
        assertEquals(memory.get(9, 1), route.getTo());
        assertEquals(expectedRoute, route.getRoute());
    }

    @Test
    public void findRouteTest1() {
        Memory memory = BaseBoardTest.board(//
                "╔═══┐" + // 4
                        "║..O│" + // 3
                        "║...│" + // 2
                        "║S.E│" + // 1
                        "└───┘", // 0
                "-----" + // 4
                        "-B↓x-" + // 3
                        "-----" + // 2
                        "-☺---" + // 1
                        "-----", // 0
                "-----" + // 4
                        "-----" + // 3
                        "-----" + // 2
                        "-----" + // 1
                        "-----"); // 0;
        ASearch search = new ASearch(memory);
        ASearchRoute route = search.findRoute(memory.getMe(), memory.get(Elements.EXIT).get(0));
        assertEquals(memory.get(1, 1), route.getFrom());
        assertEquals(memory.get(3, 1), route.getTo());
        List<Move> expectedCommands = Arrays.asList(Move.RIGHT, Move.RIGHT);
        assertEquals(expectedCommands, route.getFlatCommands());
        List<Cell> expectedRoute = List.of(memory.get(1, 1), memory.get(2, 1), memory.get(3, 1));
        assertEquals(expectedRoute, route.getRoute());
    }

    @Test
    public void findRouteTest2() {
        Memory memory = BaseBoardTest.board(//
                "╔═══┐" + // 4
                        "║..O│" + // 3
                        "║...│" + // 2
                        "║S.E│" + // 1
                        "└───┘", // 0
                "-----" + // 4
                        "-☺Bx-" + // 3
                        "---B-" + // 2
                        "-B---" + // 1
                        "-----", // 0
                "-----" + // 4
                        "-----" + // 3
                        "-----" + // 2
                        "-----" + // 1
                        "-----"); // 0;
        ASearch search = new ASearch(memory);
        ASearchRoute route = search.findRoute(memory.getMe(), memory.get(Elements.EXIT).get(0));
        List<Cell> expectedRoute = List.of(memory.get(1, 3), memory.get(1, 2), memory.get(2, 2), memory.get(2, 1),
                memory.get(3, 1));
        assertEquals(memory.get(1, 3), route.getFrom());
        assertEquals(memory.get(3, 1), route.getTo());
        assertEquals(expectedRoute, route.getRoute());
    }

    @Test
    public void findRouteJumpBoxTest() {
        Memory memory = BaseBoardTest.board(//
                "╔═══┐" + // 4
                        "║S..│" + // 3
                        "║...│" + // 2
                        "║E..│" + // 1
                        "└───┘", // 0
                "-----" + // 4
                        "-☺---" + // 3
                        "-BB--" + // 2
                        "-----" + // 1
                        "-----", // 0
                "-----" + // 4
                        "-----" + // 3
                        "-----" + // 2
                        "-----" + // 1
                        "-----"); // 0;
        ASearch search = new ASearch(memory);
        ASearchRoute route = search.findRoute(memory.getMe(), memory.get(Elements.EXIT).get(0));
        assertEquals(memory.get(1, 3), route.getFrom());
        assertEquals(memory.get(1, 1), route.getTo());
        List<ICommand> expectedCommands = List.of(Jump.DOWN);
        assertEquals(expectedCommands, route.getFlatCommands());
        List<Cell> expectedRoute = List.of(memory.get(1, 3), memory.get(1, 1));
        assertEquals(expectedRoute, route.getRoute());
    }

    @Test
    public void findRouteJumpHoleTest() {
        Memory memory = BaseBoardTest.board(//
                "╔═══┐" + // 4
                        "║S..│" + // 3
                        "║OO.│" + // 2
                        "║E..│" + // 1
                        "└───┘", // 0
                "-----" + // 4
                        "-☺---" + // 3
                        "-----" + // 2
                        "-----" + // 1
                        "-----", // 0
                "-----" + // 4
                        "-----" + // 3
                        "-----" + // 2
                        "-----" + // 1
                        "-----"); // 0;
        ASearch search = new ASearch(memory);
        ASearchRoute route = search.findRoute(memory.getMe(), memory.get(Elements.EXIT).get(0));
        assertEquals(memory.get(1, 3), route.getFrom());
        assertEquals(memory.get(1, 1), route.getTo());
        List<ICommand> expectedCommands = List.of(Jump.DOWN);
        assertEquals(expectedCommands, route.getFlatCommands());
        List<Cell> expectedRoute = List.of(memory.get(1, 3), memory.get(1, 1));
        assertEquals(expectedRoute, route.getRoute());
    }

    // cant jump through the wall
    @Test
    public void findRouteJumpWallTest() {
        Memory memory = BaseBoardTest.board(//
                "╔═══┐" + // 4
                        "║S..│" + // 3
                        "║─..│" + // 2
                        "║E..│" + // 1
                        "└───┘", // 0
                "-----" + // 4
                        "-☺---" + // 3
                        "--B--" + // 2
                        "-----" + // 1
                        "-----", // 0
                "-----" + // 4
                        "-----" + // 3
                        "-----" + // 2
                        "-----" + // 1
                        "-----"); // 0;
        ASearch search = new ASearch(memory);
        ASearchRoute route = search.findRoute(memory.getMe(), memory.get(Elements.EXIT).get(0));
        assertEquals(memory.get(1, 3), route.getFrom());
        assertEquals(memory.get(1, 1), route.getTo());
        List<ICommand> expectedCommands = List.of(Move.RIGHT, Pull.DOWN, Move.DOWN, Move.LEFT);
        assertEquals(expectedCommands, route.getFlatCommands());
        List<Cell> expectedRoute = List.of(memory.get(1, 3), memory.get(2, 3), memory.get(2, 2), memory.get(2, 1),
                memory.get(1, 1));
        assertEquals(expectedRoute, route.getRoute());
    }

    @Test
    public void findRouteJumpToLaserTest() {
        Memory memory = board(//
                "╔═════┐" + // 6
                        "║S....│" + // 5
                        "║.....│" + // 4
                        "║.....│" + // 3
                        "║.....│" + // 2
                        "║E....│" + // 1
                        "└─────┘", // 0
                "-------" + // 6
                        "-☺-----" + // 5
                        "-------" + // 4
                        "-↑-----" + // 3
                        "-------" + // 2
                        "-------" + // 1
                        "-------", // 0
                "-------" + // 6
                        "-------" + // 5
                        "-------" + // 4
                        "-------" + // 3
                        "-------" + // 2
                        "-------" + // 1
                        "-------"); // 0;
        ASearch search = new ASearch(memory);
        ASearchRoute route = search.findRoute(memory.getMe(), memory.get(Elements.EXIT).get(0));
        assertEquals(memory.get(1, 5), route.getFrom());
        assertEquals(memory.get(1, 1), route.getTo());
        List<ICommand> expectedCommands = List.of(Jump.DOWN, Move.DOWN, Move.DOWN);
        assertEquals(expectedCommands, route.getFlatCommands());
    }

}
