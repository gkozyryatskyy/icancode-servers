package com.codenjoy.dojo.icancode.asearch.pattern;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;

import com.codenjoy.dojo.icancode.BaseBoardTest;
import com.codenjoy.dojo.icancode.asearch.command.Jump;
import com.codenjoy.dojo.icancode.asearch.command.Pull;
import com.codenjoy.dojo.icancode.asearch.command.Special;
import com.codenjoy.dojo.icancode.asearch.model.Connection;
import com.codenjoy.dojo.icancode.memory.Memory;

public class BoxAndBlockPatternTest extends BaseBoardTest {

    private final BoxAndBlockPattern pattern = new BoxAndBlockPattern();

    @Test
    public void patternTest1() {
        // @formatter:off
        Memory memory = board(
                //       0123456
                "╔═════┐" + // 6
                        "║.....│" + // 5
                        "║.....│" + // 4
                        "║.└───│" + // 3
                        "║...E.│" + // 2
                        "║O....│" + // 1
                        "└─────┘", // 0
                //       0123456
                        "-------" + // 6
                        "-------" + // 5
                        "-------" + // 4
                        "-☺-----" + // 3
                        "-B-----" + // 2
                        "-------" + // 1
                        "-------", // 0
                //       0123456
                        "-------" + // 6
                        "-------" + // 5
                        "-------" + // 4
                        "-------" + // 3
                        "-------" + // 2
                        "-------" + // 1
                        "-------"); // 0;
        // @formatter:on
        Connection expected = new Connection(memory.get(1, 2),
                List.of(Pull.UP, Jump.DOWN, Special.DO_NOTHING));
        assertEquals(expected, pattern.match(memory.getMe()));
        assertEquals(expected, memory.getPattern(memory.getMe()));
    }

    @Test
    public void patternTest2() {
        // @formatter:off
        Memory memory = board(
                //       0123456
                "╔═════┐" + // 6
                        "║.║.└─│" + // 5
                        "║.║...│" + // 4
                        "║.║.╔═│" + // 3
                        "║...E.│" + // 2
                        "║.....│" + // 1
                        "└─────┘", // 0
                //       0123456
                "-------" + // 6
                        "-------" + // 5
                        "---B☺--" + // 4
                        "-------" + // 3
                        "-------" + // 2
                        "-------" + // 1
                        "-------", // 0
                //       0123456
                "-------" + // 6
                        "-------" + // 5
                        "-------" + // 4
                        "-------" + // 3
                        "-------" + // 2
                        "-------" + // 1
                        "-------"); // 0;
        // @formatter:on
        Connection expected = new Connection(memory.get(3, 4),
                List.of(Pull.RIGHT, Jump.LEFT, Special.DO_NOTHING));
        assertEquals(expected, pattern.match(memory.getMe()));
        assertEquals(expected, memory.getPattern(memory.getMe()));
    }

    @Test
    public void patternTest3() {
        // @formatter:off
        Memory memory = board(
                //       0123456
                "╔═════┐" + // 6
                        "║.....│" + // 5
                        "║.....│" + // 4
                        "║.....│" + // 3
                        "║O....│" + // 2
                        "║E....│" + // 1
                        "└─────┘", // 0
                //       0123456
                        "╔═════┐" + // 6
                        "║-----│" + // 5
                        "║☺----│" + // 4
                        "║B----│" + // 3
                        "║-----│" + // 2
                        "║-╔═══│" + // 1
                        "└─────┘", // 0
                //       0123456
                        "-------" + // 6
                        "-------" + // 5
                        "-------" + // 4
                        "-------" + // 3
                        "-------" + // 2
                        "-------" + // 1
                        "-------"); // 0;
        // @formatter:on
        Connection expected = new Connection(memory.get(1, 3),
                List.of(Pull.UP, Jump.DOWN, Special.DO_NOTHING));
        assertEquals(expected, pattern.match(memory.getMe()));
        assertEquals(expected, memory.getPattern(memory.getMe()));
    }

    @Test
    public void patternTest4() {
        // @formatter:off
        Memory memory = board(
                //       0123456
                "╔═════┐" + // 6
                        "║O..└─│" + // 5
                        "║..╔══│" + // 4
                        "║.☺║..│" + // 3
                        "║..║..│" + // 2
                        "║..║..│" + // 1
                        "└─────┘", // 0
                //       0123456
                        "╔═════┐" + // 6
                        "║-----│" + // 5
                        "║B-╔══│" + // 4
                        "║B-║--│" + // 3
                        "║-B║--│" + // 2
                        "║-B║--│" + // 1
                        "└─────┘", // 0
                //       0123456
                        "-------" + // 6
                        "-------" + // 5
                        "-------" + // 4
                        "-------" + // 3
                        "-------" + // 2
                        "-------" + // 1
                        "-------"); // 0;
        // @formatter:on
        Connection expected = new Connection(memory.get(2, 2),
                List.of(Pull.UP, Jump.DOWN, Special.DO_NOTHING));
        assertEquals(expected, pattern.match(memory.getMe()));
        assertEquals(expected, memory.getPattern(memory.getMe()));
    }

    @Test
    public void patternTest5() {
        // @formatter:off
        Memory memory = board(
                //       0123456
                "╔═════┐" + // 6
                        "║O..└─│" + // 5
                        "║.....│" + // 4
                        "║.☺╔══│" + // 3
                        "║..║..│" + // 2
                        "║.E║..│" + // 1
                        "└─────┘", // 0
                //       0123456
                "╔═════┐" + // 6
                        "║-----│" + // 5
                        "║B-╔══│" + // 4
                        "║B-║--│" + // 3
                        "║-B║--│" + // 2
                        "║--║--│" + // 1
                        "└─────┘", // 0
                //       0123456
                "-------" + // 6
                        "-------" + // 5
                        "-------" + // 4
                        "-------" + // 3
                        "-------" + // 2
                        "-------" + // 1
                        "-------"); // 0;
        // @formatter:on
        Connection expected = new Connection(memory.get(2, 2),
                List.of(Pull.UP, Jump.DOWN, Special.DO_NOTHING));
        assertEquals(expected, pattern.match(memory.getMe()));
        assertEquals(expected, memory.getPattern(memory.getMe()));
    }
}
