package com.codenjoy.dojo.icancode.asearch.pattern;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;

import com.codenjoy.dojo.icancode.BaseBoardTest;
import com.codenjoy.dojo.icancode.asearch.command.Jump;
import com.codenjoy.dojo.icancode.asearch.command.Pull;
import com.codenjoy.dojo.icancode.asearch.command.Special;
import com.codenjoy.dojo.icancode.asearch.model.Connection;
import com.codenjoy.dojo.icancode.memory.Memory;

public class TwoBoxesPatternTest extends BaseBoardTest {

    private final TwoBoxesPattern pattern = new TwoBoxesPattern();

    @Test
    public void patternTest1() {
        // @formatter:off
        Memory memory = board(
                //       0123456
                "╔═════┐" + // 6
                        "║.....│" + // 5
                        "║.....│" + // 4
                        "║.....│" + // 3
                        "║.║...│" + // 2
                        "║E....│" + // 1
                        "└─────┘", // 0
                //       0123456
                        "-------" + // 6
                        "-------" + // 5
                        "-☺-----" + // 4
                        "-B-----" + // 3
                        "-B-----" + // 2
                        "-------" + // 1
                        "-------", // 0
                //       0123456
                        "-------" + // 6
                        "-------" + // 5
                        "-------" + // 4
                        "-------" + // 3
                        "-------" + // 2
                        "-------" + // 1
                        "-------"); // 0;
        // @formatter:on
        Connection expected = new Connection(memory.get(1, 2),
                List.of(Pull.UP, Jump.DOWN, Special.DO_NOTHING, Jump.DOWN, Special.DO_NOTHING, Pull.UP));
        assertEquals(expected, pattern.match(memory.getMe()));
        assertEquals(expected, memory.getPattern(memory.getMe()));
    }

    @Test
    public void patternTest2() {
        // @formatter:off
        Memory memory = board(
                //       0123456
                "╔═════┐" + // 6
                        "║...═.│" + // 5
                        "║....E│" + // 4
                        "║...═.│" + // 3
                        "║.....│" + // 2
                        "║.....│" + // 1
                        "└─────┘", // 0
                //       0123456
                        "-------" + // 6
                        "-------" + // 5
                        "--☺BB--" + // 4
                        "-------" + // 3
                        "-------" + // 2
                        "-------" + // 1
                        "-------", // 0
                //       0123456
                        "-------" + // 6
                        "-------" + // 5
                        "-------" + // 4
                        "-------" + // 3
                        "-------" + // 2
                        "-------" + // 1
                        "-------"); // 0;
        // @formatter:on
        Connection expected = new Connection(memory.get(4, 4),
                List.of(Pull.LEFT, Jump.RIGHT, Special.DO_NOTHING, Jump.RIGHT, Special.DO_NOTHING,
                        Pull.LEFT));
        assertEquals(expected, pattern.match(memory.getMe()));
        assertEquals(expected, memory.getPattern(memory.getMe()));
    }

    @Test // check blocked by two boxes from each side
    public void patternTest3() {
        // @formatter:off
        Memory memory = board(
                //       0123456
                "╔═════┐" + // 6
                        "║.....│" + // 5
                        "║.....│" + // 4
                        "║.....│" + // 3
                        "║.....│" + // 2
                        "║..E..│" + // 1
                        "└─────┘", // 0
                //       0123456
                "-------" + // 6
                        "-------" + // 5
                        "---☺---" + // 4
                        "--BBB--" + // 3
                        "--BBB--" + // 2
                        "-------" + // 1
                        "-------", // 0
                //       0123456
                "-------" + // 6
                        "-------" + // 5
                        "-------" + // 4
                        "-------" + // 3
                        "-------" + // 2
                        "-------" + // 1
                        "-------"); // 0;
        // @formatter:on
        Connection expected = new Connection(memory.get(3, 2),
                List.of(Pull.UP, Jump.DOWN, Special.DO_NOTHING, Jump.DOWN, Special.DO_NOTHING, Pull.UP));
        assertEquals(expected, pattern.match(memory.getMe()));
        assertEquals(expected, memory.getPattern(memory.getMe()));
    }

    @Test
    public void patternTest4() {
        // @formatter:off
        Memory memory = board(
                //       0123456
                "╔══════┐" + // 7
                        "║......│" + // 6
                        "║......│" + // 5
                        "║......│" + // 4
                        "║..O...│" + // 3
                        "║──┘.└─│" + // 2
                        "║E.....│" + // 1
                        "└──────┘", // 0
                //       01234567
                "--------" + // 7
                        "--------" + // 6
                        "----☺---" + // 5
                        "---BBB--" + // 4
                        "----B---" + // 3
                        "--------" + // 2
                        "--------" + // 1
                        "--------", // 0
                //       01234567
                "--------" + // 7
                        "--------" + // 6
                        "--------" + // 5
                        "--------" + // 4
                        "--------" + // 3
                        "--------" + // 2
                        "--------" + // 1
                        "--------"); // 0;
        // @formatter:on
        Connection expected = new Connection(memory.get(4, 3),
                List.of(Pull.UP, Jump.DOWN, Special.DO_NOTHING, Jump.DOWN, Special.DO_NOTHING, Pull.UP));
        assertEquals(expected, pattern.match(memory.getMe()));
        assertEquals(expected, memory.getPattern(memory.getMe()));
    }

    @Test
    public void patternTest5() {
        // @formatter:off
        Memory memory = board(
                //       0123456
                "╔══════┐" + // 7
                        "║......│" + // 6
                        "║──┘.└─│" + // 5
                        "║......│" + // 4
                        "║......│" + // 3
                        "║......│" + // 2
                        "║E.....│" + // 1
                        "└──────┘", // 0
                //       01234567
                "--------" + // 7
                        "--------" + // 6
                        "----☺---" + // 5
                        "----BB--" + // 4
                        "----B---" + // 3
                        "--------" + // 2
                        "--------" + // 1
                        "--------", // 0
                //       01234567
                "--------" + // 7
                        "--------" + // 6
                        "--------" + // 5
                        "--------" + // 4
                        "--------" + // 3
                        "--------" + // 2
                        "--------" + // 1
                        "--------"); // 0;
        // @formatter:on
        Connection expected = new Connection(memory.get(4, 3),
                List.of(Pull.UP, Jump.DOWN, Special.DO_NOTHING, Jump.DOWN, Special.DO_NOTHING, Pull.UP));
        assertEquals(expected, pattern.match(memory.getMe()));
        assertEquals(expected, memory.getPattern(memory.getMe()));
    }

}
