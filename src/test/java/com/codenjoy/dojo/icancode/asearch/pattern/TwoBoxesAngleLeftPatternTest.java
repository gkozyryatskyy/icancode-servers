package com.codenjoy.dojo.icancode.asearch.pattern;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;

import com.codenjoy.dojo.icancode.BaseBoardTest;
import com.codenjoy.dojo.icancode.asearch.ASearch;
import com.codenjoy.dojo.icancode.asearch.command.ICommand;
import com.codenjoy.dojo.icancode.asearch.command.Jump;
import com.codenjoy.dojo.icancode.asearch.command.Pull;
import com.codenjoy.dojo.icancode.asearch.command.Special;
import com.codenjoy.dojo.icancode.asearch.model.Connection;
import com.codenjoy.dojo.icancode.memory.Memory;
import com.codenjoy.dojo.icancode.strategy.GoldStrategy;
import com.codenjoy.dojo.icancode.strategy.StrategyResult;

public class TwoBoxesAngleLeftPatternTest extends BaseBoardTest {

    private final TwoBoxesAngleLeftPattern pattern = new TwoBoxesAngleLeftPattern();

    @Test
    public void patternWithStrategyTest1() {
        // @formatter:off
        Memory memory = board(
                //       0123456
                "╔═════┐" + // 6
                        "║...│.│" + // 5
                        "║...│.│" + // 4
                        "║...└─│" + // 3
                        "║.....│" + // 2
                        "║E..╔═│" + // 1
                        "└─────┘", // 0
                //       0123456
                "-------" + // 6
                        "-------" + // 5
                        "-------" + // 4
                        "-------" + // 3
                        "----B☺-" + // 2
                        "---B---" + // 1
                        "-------", // 0
                //       0123456
                "-------" + // 6
                        "-------" + // 5
                        "-------" + // 4
                        "-------" + // 3
                        "-------" + // 2
                        "-------" + // 1
                        "-------"); // 0;
        // @formatter:on
        // pattern test
        List<ICommand> expectedC = List.of(Jump.LEFT, Special.DO_NOTHING, Pull.UP);
        assertEquals(new Connection(memory.get(3, 3), expectedC), pattern.match(memory.getMe()));
        // strategy test
        GoldStrategy st = new GoldStrategy();
        StrategyResult res = st.result(new ASearch(memory));
        assertEquals(expectedC, res.getCommands());
    }
}
