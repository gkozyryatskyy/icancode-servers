package com.codenjoy.dojo.icancode.asearch.pattern;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;

import com.codenjoy.dojo.icancode.BaseBoardTest;
import com.codenjoy.dojo.icancode.asearch.ASearch;
import com.codenjoy.dojo.icancode.asearch.command.ICommand;
import com.codenjoy.dojo.icancode.asearch.command.Jump;
import com.codenjoy.dojo.icancode.asearch.command.Pull;
import com.codenjoy.dojo.icancode.asearch.command.Special;
import com.codenjoy.dojo.icancode.asearch.model.Connection;
import com.codenjoy.dojo.icancode.memory.Memory;
import com.codenjoy.dojo.icancode.strategy.GoldStrategy;
import com.codenjoy.dojo.icancode.strategy.StrategyResult;

public class TwoBoxesInAnglePatternTest extends BaseBoardTest {

    private final TwoBoxesInAnglePattern pattern = new TwoBoxesInAnglePattern();

    @Test
    public void patternWithStrategyTest1() {
        // @formatter:off
        Memory memory = board(
                //       0123456
                "╔═════┐" + // 6
                        "║.....│" + // 5
                        "║.╔══.│" + // 4
                        "║.│...│" + // 3
                        "║─┘...│" + // 2
                        "║E..╔═│" + // 1
                        "└─────┘", // 0
                //       0123456
                "-------" + // 6
                        "-------" + // 5
                        "-------" + // 4
                        "-------" + // 3
                        "---☺---" + // 2
                        "--BB---" + // 1
                        "-------", // 0
                //       0123456
                "-------" + // 6
                        "-------" + // 5
                        "-------" + // 4
                        "-------" + // 3
                        "-------" + // 2
                        "-------" + // 1
                        "-------"); // 0;
        // @formatter:on
        // pattern test
        List<ICommand> expectedC = List.of(Pull.UP, Jump.DOWN, Special.DO_NOTHING, Jump.LEFT,
                Special.DO_NOTHING, Pull.RIGHT);
        assertEquals(new Connection(memory.get(2, 1), expectedC), pattern.match(memory.getMe()));
        // strategy test
        GoldStrategy st = new GoldStrategy();
        StrategyResult res = st.result(new ASearch(memory));
        assertEquals(expectedC, res.getCommands());
    }
}
