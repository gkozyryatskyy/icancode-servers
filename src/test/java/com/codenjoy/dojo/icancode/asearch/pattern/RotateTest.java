package com.codenjoy.dojo.icancode.asearch.pattern;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

import com.codenjoy.dojo.icancode.asearch.command.Act;
import com.codenjoy.dojo.icancode.asearch.command.Move;
import com.codenjoy.dojo.icancode.asearch.command.Pull;

public class RotateTest {

    @Test
    public void rotate90Test() {
        Rotate rotate = Rotate.ROTATE_90;
        assertEquals(new XY(3, -5), rotate.rotateFunc().apply(new XY(5, 3)));
        assertEquals(List.of(Move.DOWN, Move.RIGHT, Move.UP, Move.LEFT),
                rotate.rotateCommandsFunc().apply(List.of(Move.RIGHT, Move.UP, Move.LEFT, Move.DOWN)));
        assertEquals(List.of(Act.DOWN, Act.RIGHT, Act.UP, Act.LEFT),
                rotate.rotateCommandsFunc().apply(List.of(Act.RIGHT, Act.UP, Act.LEFT, Act.DOWN)));
        assertEquals(List.of(Pull.DOWN, Pull.RIGHT, Pull.UP, Pull.LEFT),
                rotate.rotateCommandsFunc().apply(List.of(Pull.RIGHT, Pull.UP, Pull.LEFT, Pull.DOWN)));
    }

    @Test
    public void rotate180Test() {
        Rotate rotate = Rotate.ROTATE_180;
        assertEquals(new XY(-5, -3), rotate.rotateFunc().apply(new XY(5, 3)));
        assertEquals(List.of(Move.LEFT, Move.DOWN, Move.RIGHT, Move.UP),
                rotate.rotateCommandsFunc().apply(List.of(Move.RIGHT, Move.UP, Move.LEFT, Move.DOWN)));
        assertEquals(List.of(Act.LEFT, Act.DOWN, Act.RIGHT, Act.UP),
                rotate.rotateCommandsFunc().apply(List.of(Act.RIGHT, Act.UP, Act.LEFT, Act.DOWN)));
        assertEquals(List.of(Pull.LEFT, Pull.DOWN, Pull.RIGHT, Pull.UP),
                rotate.rotateCommandsFunc().apply(List.of(Pull.RIGHT, Pull.UP, Pull.LEFT, Pull.DOWN)));
    }

    @Test
    public void rotate270Test() {
        Rotate rotate = Rotate.ROTATE_270;
        assertEquals(new XY(-3, 5), rotate.rotateFunc().apply(new XY(5, 3)));
        assertEquals(List.of(Move.UP, Move.LEFT, Move.DOWN, Move.RIGHT),
                rotate.rotateCommandsFunc().apply(List.of(Move.RIGHT, Move.UP, Move.LEFT, Move.DOWN)));
        assertEquals(List.of(Act.UP, Act.LEFT, Act.DOWN, Act.RIGHT),
                rotate.rotateCommandsFunc().apply(List.of(Act.RIGHT, Act.UP, Act.LEFT, Act.DOWN)));
        assertEquals(List.of(Pull.UP, Pull.LEFT, Pull.DOWN, Pull.RIGHT),
                rotate.rotateCommandsFunc().apply(List.of(Pull.RIGHT, Pull.UP, Pull.LEFT, Pull.DOWN)));
    }
}
