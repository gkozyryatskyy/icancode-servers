package com.codenjoy.dojo.icancode.asearch.pattern;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;

import com.codenjoy.dojo.icancode.BaseBoardTest;
import com.codenjoy.dojo.icancode.asearch.command.Jump;
import com.codenjoy.dojo.icancode.asearch.command.Pull;
import com.codenjoy.dojo.icancode.asearch.command.Special;
import com.codenjoy.dojo.icancode.asearch.model.Connection;
import com.codenjoy.dojo.icancode.memory.Memory;

public class BoxInAngleLeftPatternTest extends BaseBoardTest {

    private final BoxInAngleLeftPattern pattern = new BoxInAngleLeftPattern();

    @Test
    public void patternTest() {
        // @formatter:off
        Memory memory = board(
                //       0123456
                "╔═════┐" + // 6
                        "║O....│" + // 5
                        "║.....│" + // 4
                        "║..☺└─│" + // 3
                        "║.║...│" + // 2
                        "║.└───│" + // 1
                        "└─────┘", // 0
                //       0123456
                        "╔═════┐" + // 6
                        "║-----│" + // 5
                        "║-----│" + // 4
                        "║---└─│" + // 3
                        "║-║B--│" + // 2
                        "║-└───│" + // 1
                        "└─────┘", // 0
                //       0123456
                        "-------" + // 6
                        "-------" + // 5
                        "-------" + // 4
                        "-------" + // 3
                        "-------" + // 2
                        "-------" + // 1
                        "-------"); // 0;
        // @formatter:on
        Connection expected = new Connection(memory.get(3, 2), List.of(Pull.UP, Jump.DOWN, Special.DO_NOTHING));
        assertEquals(expected, pattern.match(memory.getMe()));
        assertEquals(expected, memory.getPattern(memory.getMe()));
    }
}
