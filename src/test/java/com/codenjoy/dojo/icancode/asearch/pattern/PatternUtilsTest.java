package com.codenjoy.dojo.icancode.asearch.pattern;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.codenjoy.dojo.icancode.BaseBoardTest;
import com.codenjoy.dojo.icancode.memory.Memory;

public class PatternUtilsTest {

    @Test
    public void isBlockedTest() {
        Memory memory = BaseBoardTest.board(//
                "╔═══┐" + // 4
                        "║S..│" + // 3
                        "║OO.│" + // 2
                        "║E..│" + // 1
                        "└───┘", // 0
                "-----" + // 4
                        "-☺---" + // 3
                        "-B---" + // 2
                        "-B---" + // 1
                        "-----", // 0
                "-----" + // 4
                        "-----" + // 3
                        "-----" + // 2
                        "-----" + // 1
                        "-----"); // 0;
        assertTrue(PatternUtils.isBlocked(memory.get(1, 0), memory.get(1, 1)));
        assertTrue(PatternUtils.isBlocked(memory.get(1, 1), memory.get(1, 2)));
        assertTrue(PatternUtils.isBlocked(memory.get(1, 0), memory.get(1, 1), memory.get(1, 2)));
        assertFalse(PatternUtils.isBlocked(memory.get(1, 1), memory.get(2, 1)));
    }

    @Test
    public void isBlocked2Test() {
        // @formatter:off
        Memory memory = BaseBoardTest.board(
                //       0123456
                "╔══════┐" + // 7
                        "║......│" + // 6
                        "║......│" + // 5
                        "║......│" + // 4
                        "║..O...│" + // 3
                        "║──┘.└─│" + // 2
                        "║E.....│" + // 1
                        "└──────┘", // 0
                //       01234567
                "--------" + // 7
                        "--------" + // 6
                        "----☺---" + // 5
                        "---BBB--" + // 4
                        "----B---" + // 3
                        "--------" + // 2
                        "--------" + // 1
                        "--------", // 0
                //       01234567
                "--------" + // 7
                        "--------" + // 6
                        "--------" + // 5
                        "--------" + // 4
                        "--------" + // 3
                        "--------" + // 2
                        "--------" + // 1
                        "--------"); // 0;
        // @formatter:on
        assertTrue(PatternUtils.isBlocked(memory.get(3, 6), memory.get(3, 5), memory.get(3, 4), memory.get(3, 3),
                memory.get(3, 2)));
        assertTrue(PatternUtils.isBlocked(memory.get(5, 6), memory.get(5, 5), memory.get(5, 4), memory.get(5, 3),
                memory.get(5, 2)));
    }

    @Test(expected = IllegalArgumentException.class)
    public void isBlockedTestError() {
        Memory memory = BaseBoardTest.board(//
                "╔═══┐" + // 4
                        "║S..│" + // 3
                        "║OO.│" + // 2
                        "║E..│" + // 1
                        "└───┘", // 0
                "-----" + // 4
                        "-☺---" + // 3
                        "-B---" + // 2
                        "-B---" + // 1
                        "-----", // 0
                "-----" + // 4
                        "-----" + // 3
                        "-----" + // 2
                        "-----" + // 1
                        "-----"); // 0;
        PatternUtils.isBlocked(memory.get(1, 1), memory.get(3, 1));
    }
}
