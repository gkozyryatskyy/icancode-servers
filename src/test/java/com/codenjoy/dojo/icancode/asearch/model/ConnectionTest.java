package com.codenjoy.dojo.icancode.asearch.model;

import static org.junit.Assert.assertEquals;

import java.util.Set;
import java.util.stream.Collectors;

import org.junit.Test;

import com.codenjoy.dojo.icancode.BaseBoardTest;
import com.codenjoy.dojo.icancode.asearch.command.Jump;
import com.codenjoy.dojo.icancode.memory.Memory;

public class ConnectionTest {

    @Test
    public void pullTest() {
        // @formatter:off
        Memory memory = BaseBoardTest.board(
                //       0123456
                "╔═════┐" + // 6
                        "║...│.│" + // 5
                        "║...│.│" + // 4
                        "║...└─│" + // 3
                        "║.....│" + // 2
                        "║.....│" + // 1
                        "└─────┘", // 0
                //       0123456
                "╔═════┐" + // 6
                        "║-----│" + // 5
                        "║-♀-BX│" + // 4
                        "║BB☺←&│" + // 3
                        "║───╗B│" + // 2
                        "║----E│" + // 1
                        "└─────┘", // 0
                //       0123456
                "-------" + // 6
                        "-------" + // 5
                        "-------" + // 4
                        "-------" + // 3
                        "-------" + // 2
                        "-------" + // 1
                        "-------"); // 0;
        // @formatter:on
        assertEquals(Set.of(Jump.UP, Jump.JUMP), new Connection(memory.getMe()).getConnections()
                .stream()
                .flatMap(e -> e.getCommands().stream())
                .collect(Collectors.toSet()));
    }
}
