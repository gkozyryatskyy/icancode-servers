package com.codenjoy.dojo.icancode.asearch;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import org.junit.Test;

import com.codenjoy.dojo.icancode.BaseBoardTest;
import com.codenjoy.dojo.icancode.asearch.command.Jump;
import com.codenjoy.dojo.icancode.asearch.command.Move;
import com.codenjoy.dojo.icancode.memory.Memory;
import com.codenjoy.dojo.icancode.model.Elements;

public class ASearchLaserZombieDodgeTest {

    @Test
    public void dodgeLeftLasers() {
        Memory memory = BaseBoardTest.board(//
                "╔═══┐" + // 4
                        "║.S.│" + // 3
                        "║...│" + // 2
                        "║.E.│" + // 1
                        "└───┘", // 0
                "-----" + // 4
                        "--☺--" + // 3
                        "---←-" + // 2
                        "-----" + // 1
                        "-----", // 0
                "-----" + // 4
                        "-----" + // 3
                        "-----" + // 2
                        "-----" + // 1
                        "-----"); // 0
        ASearch search = new ASearch(memory);
        ASearchRoute route = search.findRoute(memory.getMe(), memory.get(Elements.EXIT).get(0));
        assertNotEquals(Move.DOWN, route.getNextCommand());
    }

    @Test
    public void dodgeRightLasers() {
        Memory memory = BaseBoardTest.board(//
                "╔═══┐" + // 4
                        "║.S.│" + // 3
                        "║...│" + // 2
                        "║.E.│" + // 1
                        "└───┘", // 0
                "-----" + // 4
                        "--☺--" + // 3
                        "-→---" + // 2
                        "-----" + // 1
                        "-----", // 0
                "-----" + // 4
                        "-----" + // 3
                        "-----" + // 2
                        "-----" + // 1
                        "-----"); // 0
        ASearch search = new ASearch(memory);
        ASearchRoute route = search.findRoute(memory.getMe(), memory.get(Elements.EXIT).get(0));
        assertNotEquals(Move.DOWN, route.getNextCommand());
    }

    @Test
    public void dodgeUpLasers() {
        Memory memory = BaseBoardTest.board(//
                "╔═══┐" + // 4
                        "║...│" + // 3
                        "║S.E│" + // 2
                        "║...│" + // 1
                        "└───┘", // 0
                "-----" + // 4
                        "-----" + // 3
                        "-☺---" + // 2
                        "--↑--" + // 1
                        "-----", // 0
                "-----" + // 4
                        "-----" + // 3
                        "-----" + // 2
                        "-----" + // 1
                        "-----"); // 0
        ASearch search = new ASearch(memory);
        ASearchRoute route = search.findRoute(memory.getMe(), memory.get(Elements.EXIT).get(0));
        assertNotEquals(Move.RIGHT, route.getNextCommand());
    }

    @Test
    public void dodgeDownLasers() {
        Memory memory = BaseBoardTest.board(//
                "╔═══┐" + // 4
                        "║...│" + // 3
                        "║S.E│" + // 2
                        "║...│" + // 1
                        "└───┘", // 0
                "-----" + // 4
                        "--↓--" + // 3
                        "-☺---" + // 2
                        "-----" + // 1
                        "-----", // 0
                "-----" + // 4
                        "-----" + // 3
                        "-----" + // 2
                        "-----" + // 1
                        "-----"); // 0
        ASearch search = new ASearch(memory);
        ASearchRoute route = search.findRoute(memory.getMe(), memory.get(Elements.EXIT).get(0));
        assertNotEquals(Move.RIGHT, route.getNextCommand());
    }

    @Test
    public void dodgeLeftLaserMachine() {
        Memory memory = BaseBoardTest.board(//
                "╔═══┐" + // 4
                        "║.S.│" + // 3
                        "║..◄│" + // 2
                        "║.E.│" + // 1
                        "└───┘", // 0
                "-----" + // 4
                        "--☺--" + // 3
                        "-----" + // 2
                        "-----" + // 1
                        "-----", // 0
                "-----" + // 4
                        "-----" + // 3
                        "-----" + // 2
                        "-----" + // 1
                        "-----"); // 0
        ASearch search = new ASearch(memory);
        ASearchRoute route = search.findRoute(memory.getMe(), memory.get(Elements.EXIT).get(0));
        assertNotEquals(Move.DOWN, route.getNextCommand());
    }

    @Test
    public void dodgeRightLaserMachine() {
        Memory memory = BaseBoardTest.board(//
                "╔═══┐" + // 4
                        "║.S.│" + // 3
                        "║►..│" + // 2
                        "║.E.│" + // 1
                        "└───┘", // 0
                "-----" + // 4
                        "--☺--" + // 3
                        "-----" + // 2
                        "-----" + // 1
                        "-----", // 0
                "-----" + // 4
                        "-----" + // 3
                        "-----" + // 2
                        "-----" + // 1
                        "-----"); // 0
        ASearch search = new ASearch(memory);
        ASearchRoute route = search.findRoute(memory.getMe(), memory.get(Elements.EXIT).get(0));
        assertNotEquals(Move.DOWN, route.getNextCommand());
    }

    @Test
    public void dodgeUpLaserMachine() {
        Memory memory = BaseBoardTest.board(//
                "╔═══┐" + // 4
                        "║...│" + // 3
                        "║S.E│" + // 2
                        "║.▲.│" + // 1
                        "└───┘", // 0
                "-----" + // 4
                        "-----" + // 3
                        "-☺---" + // 2
                        "-----" + // 1
                        "-----", // 0
                "-----" + // 4
                        "-----" + // 3
                        "-----" + // 2
                        "-----" + // 1
                        "-----"); // 0
        ASearch search = new ASearch(memory);
        ASearchRoute route = search.findRoute(memory.getMe(), memory.get(Elements.EXIT).get(0));
        assertNotEquals(Move.RIGHT, route.getNextCommand());
    }

    @Test
    public void dodgeDownLaserMachine() {
        Memory memory = BaseBoardTest.board(//
                "╔═══┐" + // 4
                        "║.▼.│" + // 3
                        "║S.E│" + // 2
                        "║...│" + // 1
                        "└───┘", // 0
                "-----" + // 4
                        "-----" + // 3
                        "-☺---" + // 2
                        "-----" + // 1
                        "-----", // 0
                "-----" + // 4
                        "-----" + // 3
                        "-----" + // 2
                        "-----" + // 1
                        "-----"); // 0
        ASearch search = new ASearch(memory);
        ASearchRoute route = search.findRoute(memory.getMe(), memory.get(Elements.EXIT).get(0));
        assertNotEquals(Move.RIGHT, route.getNextCommand());
    }

    @Test
    public void dodgeFemaleZombie() {
        Memory memory = BaseBoardTest.board(//
                "╔═══┐" + // 4
                        "║...│" + // 3
                        "║S.E│" + // 2
                        "║...│" + // 1
                        "└───┘", // 0
                "-----" + // 4
                        "--♀--" + // 3
                        "-☺---" + // 2
                        "-----" + // 1
                        "-----", // 0
                "-----" + // 4
                        "-----" + // 3
                        "-----" + // 2
                        "-----" + // 1
                        "-----"); // 0
        ASearch search = new ASearch(memory);
        ASearchRoute route = search.findRoute(memory.getMe(), memory.get(Elements.EXIT).get(0));
        assertNotEquals(Move.RIGHT, route.getNextCommand());
    }

    @Test
    public void dodgeMaleZombie() {
        Memory memory = BaseBoardTest.board(//
                "╔═══┐" + // 4
                        "║...│" + // 3
                        "║S.E│" + // 2
                        "║...│" + // 1
                        "└───┘", // 0
                "-----" + // 4
                        "--♂--" + // 3
                        "-☺---" + // 2
                        "-----" + // 1
                        "-----", // 0
                "-----" + // 4
                        "-----" + // 3
                        "-----" + // 2
                        "-----" + // 1
                        "-----"); // 0
        ASearch search = new ASearch(memory);
        ASearchRoute route = search.findRoute(memory.getMe(), memory.get(Elements.EXIT).get(0));
        assertNotEquals(Move.RIGHT, route.getNextCommand());
    }

    @Test
    public void dodgeOtherRoboLaser() {
        Memory memory = BaseBoardTest.board(//
                "╔═══┐" + // 4
                        "║...│" + // 3
                        "║S.E│" + // 2
                        "║...│" + // 1
                        "└───┘", // 0
                "-----" + // 4
                        "--X--" + // 3
                        "-☺---" + // 2
                        "-----" + // 1
                        "-----", // 0
                "-----" + // 4
                        "-----" + // 3
                        "-----" + // 2
                        "-----" + // 1
                        "-----"); // 0
        ASearch search = new ASearch(memory);
        ASearchRoute route = search.findRoute(memory.getMe(), memory.get(Elements.EXIT).get(0));
        assertNotEquals(Move.RIGHT, route.getNextCommand());
    }

    @Test
    public void goToNotDirectRightLaser() {
        Memory memory = BaseBoardTest.board(//
                "╔═══┐" + // 4
                        "║.E.│" + // 3
                        "║...│" + // 2
                        "║.S.│" + // 1
                        "└───┘", // 0
                "-----" + // 4
                        "-----" + // 3
                        "--→--" + // 2
                        "--☺--" + // 1
                        "-----", // 0
                "-----" + // 4
                        "-----" + // 3
                        "-----" + // 2
                        "-----" + // 1
                "-----"); // 0
        ASearch search = new ASearch(memory);
        ASearchRoute route = search.findRoute(memory.getMe(), memory.get(Elements.EXIT).get(0));
        assertEquals(Move.UP, route.getNextCommand());
    }

    @Test
    public void goToNotDirectUpLaser() {
        Memory memory = BaseBoardTest.board(//
                "╔═══┐" + // 4
                        "║...│" + // 3
                        "║S.E│" + // 2
                        "║...│" + // 1
                        "└───┘", // 0
                "-----" + // 4
                        "-----" + // 3
                        "-☺↑--" + // 2
                        "-----" + // 1
                        "-----", // 0
                "-----" + // 4
                        "-----" + // 3
                        "-----" + // 2
                        "-----" + // 1
                        "-----"); // 0
        ASearch search = new ASearch(memory);
        ASearchRoute route = search.findRoute(memory.getMe(), memory.get(Elements.EXIT).get(0));
        assertEquals(Move.RIGHT, route.getNextCommand());
    }

    @Test
    public void goToNotDirectDownLaser() {
        Memory memory = BaseBoardTest.board(//
                "╔═══┐" + // 4
                        "║...│" + // 3
                        "║S.E│" + // 2
                        "║...│" + // 1
                        "└───┘", // 0
                "-----" + // 4
                        "-----" + // 3
                        "-☺↓--" + // 2
                        "-----" + // 1
                        "-----", // 0
                "-----" + // 4
                        "-----" + // 3
                        "-----" + // 2
                        "-----" + // 1
                        "-----"); // 0
        ASearch search = new ASearch(memory);
        ASearchRoute route = search.findRoute(memory.getMe(), memory.get(Elements.EXIT).get(0));
        // using Jump.RIGHT because ↓ laser hides all other lasers
        assertEquals(Jump.RIGHT, route.getNextCommand());
    }

    @Test
    public void notGoToDirectLeftLaser() {
        Memory memory = BaseBoardTest.board(//
                "╔═══┐" + // 4
                        "║...│" + // 3
                        "║S.E│" + // 2
                        "║...│" + // 1
                        "└───┘", // 0
                "-----" + // 4
                        "-----" + // 3
                        "-☺←--" + // 2
                        "-----" + // 1
                        "-----", // 0
                "-----" + // 4
                        "-----" + // 3
                        "-----" + // 2
                        "-----" + // 1
                        "-----"); // 0
        ASearch search = new ASearch(memory);
        ASearchRoute route = search.findRoute(memory.getMe(), memory.get(Elements.EXIT).get(0));
        assertNotEquals(Move.RIGHT, route.getNextCommand());
    }

    @Test // we can jump and next turn laser machine will be charged and we will be dead
    public void doNotJumpToChargingLaserMachinePointedToYou() {
        Memory memory = BaseBoardTest.board(//
                "╔═════┐" + // 6
                        "║S....│" + // 5
                        "║O....│" + // 4
                        "║E....│" + // 3
                        "║˄....│" + // 2
                        "║.....│" + // 1
                        "└─────┘", // 0
                "-------" + // 6
                        "-☺-----" + // 5
                        "-------" + // 4
                        "-------" + // 3
                        "-------" + // 2
                        "-------" + // 1
                        "-------", // 0
                "-------" + // 6
                        "-------" + // 5
                        "-------" + // 4
                        "-------" + // 3
                        "-------" + // 2
                        "-------" + // 1
                        "-------"); // 0;
        ASearch search = new ASearch(memory);
        ASearchRoute route = search.findRoute(memory.getMe(), memory.get(Elements.EXIT).get(0));
        assertNotEquals(Jump.DOWN, route.getNextCommand());
    }

    @Test
    public void jumpToChargingLaserMachineNotPointedToYou() {
        Memory memory = BaseBoardTest.board(//
                "╔═════┐" + // 6
                        "║S....│" + // 5
                        "║O....│" + // 4
                        "║E....│" + // 3
                        "║˃....│" + // 2
                        "║.....│" + // 1
                        "└─────┘", // 0
                "-------" + // 6
                        "-☺-----" + // 5
                        "-------" + // 4
                        "-------" + // 3
                        "-------" + // 2
                        "-------" + // 1
                        "-------", // 0
                "-------" + // 6
                        "-------" + // 5
                        "-------" + // 4
                        "-------" + // 3
                        "-------" + // 2
                        "-------" + // 1
                        "-------"); // 0;
        ASearch search = new ASearch(memory);
        ASearchRoute route = search.findRoute(memory.getMe(), memory.get(Elements.EXIT).get(0));
        assertEquals(Jump.DOWN, route.getNextCommand());
    }
}
