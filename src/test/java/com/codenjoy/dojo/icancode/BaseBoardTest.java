package com.codenjoy.dojo.icancode;

import com.codenjoy.dojo.icancode.client.ExtendedBoard;
import com.codenjoy.dojo.icancode.memory.LongMemory;
import com.codenjoy.dojo.icancode.memory.Memory;

public class BaseBoardTest {

    public static final String[] defaultBoard = {//
            "╔═════════┐" +  // 10
                    "║....◄....│" + // 9
                    "║.S.┌─╗...│" + // 8
                    "║...│ ║˄.$│" + // 7
                    "║.┌─┘ └─╗&│" + // 6
                    "║.│     ║.│" + // 5
                    "║.╚═┐ ╔═╝$│" + // 4
                    "║lO.│ ║..O│" + // 3
                    "║r..╚═╝...│" + // 2
                    "║O.$..f..E│" + // 1
                    "└─────────┘", // 0
            "-----------" + //
                    "------↑--^-" + //
                    "--☺--------" + //
                    "--B--------" + //
                    "---------←-" + //
                    "-----------" + //
                    "---------→-" + //
                    "-------B↓x-" + //
                    "--------X--" + //
                    "--B--------" + //
                    "-----------", //
            "-----------" + //
                    "-----------" + //
                    "-----------" + //
                    "-----------" + //
                    "-----------" + //
                    "-----------" + //
                    "-----------" + //
                    "-----------" + //
                    "-----------" + //
                    "-----------" + //
                    "-----------" }; //

    public static Memory board(String... layers) {
        return board(new LongMemory(), layers);
    }

    public static Memory board(LongMemory mem, String... layers) {
        return new Memory((ExtendedBoard) new ExtendedBoard(true).forString(layers), mem);
    }

    public static final Memory board7x7 = board(//
            "╔═════┐" + // 6
                    "║S....│" + // 5
                    "║.....│" + // 4
                    "║.....│" + // 3
                    "║.....│" + // 2
                    "║E....│" + // 1
                    "└─────┘", // 0
            "-------" + // 6
                    "-☺-----" + // 5
                    "-------" + // 4
                    "-------" + // 3
                    "-------" + // 2
                    "-------" + // 1
                    "-------", // 0
            "-------" + // 6
                    "-------" + // 5
                    "-------" + // 4
                    "-------" + // 3
                    "-------" + // 2
                    "-------" + // 1
                    "-------"); // 0;
}
