package com.codenjoy.dojo.icancode.strategy;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.junit.Test;

import com.codenjoy.dojo.icancode.BaseBoardTest;
import com.codenjoy.dojo.icancode.asearch.ASearch;
import com.codenjoy.dojo.icancode.asearch.ASearchRoute;
import com.codenjoy.dojo.icancode.asearch.command.Jump;
import com.codenjoy.dojo.icancode.asearch.command.Move;
import com.codenjoy.dojo.icancode.memory.Memory;

public class GoldStrategyTest {

    private static final GoldStrategy strategy = new GoldStrategy();

    @Test
    public void doNoGoToGoldThroughFinish() {
        Memory memory = BaseBoardTest.board(//
                "╔═══┐" + // 4
                        "║..O│" + // 3
                        "║...│" + // 2
                        "║SE$│" + // 1
                        "└───┘", // 0
                "-----" + // 4
                        "-----" + // 3
                        "-----" + // 2
                        "-☺---" + // 1
                        "-----", // 0
                "-----" + // 4
                        "-----" + // 3
                        "-----" + // 2
                        "-----" + // 1
                        "-----"); // 0
        ASearch search = new ASearch(memory);
        StrategyResult command = strategy.result(search);
        assertEquals(List.of(Jump.RIGHT), command.getCommands());
    }

    //do not go in cycle when we are jumping around and other player is staying in the middle
    @Test
    public void doNotGoInCycle() {
        Memory memory = BaseBoardTest.board(//
                "╔═══┐" + // 4
                        "║...│" + // 3
                        "║$.$│" + // 2
                        "║S.E│" + // 1
                        "└───┘", // 0
                "-----" + // 4
                        "-----" + // 3
                        "--X--" + // 2
                        "-☺---" + // 1
                        "-----", // 0
                "-----" + // 4
                        "-----" + // 3
                        "-----" + // 2
                        "-----" + // 1
                        "-----"); // 0
        ASearch search = new ASearch(memory);
        StrategyResult command = strategy.result(search);
        assertNotEquals(Arrays.asList(Move.RIGHT, Move.RIGHT), command.getCommands());
    }

    @Test
    public void goToNearGold() {
        // check that "near" calculating from route length, but not from field positions
        Memory memory = BaseBoardTest.board(//
                "╔═══┐" + // 4
                        "║..O│" + // 3
                        "║...│" + // 2
                        "║S│$│" + // 1
                        "└───┘", // 0
                "-----" + // 4
                        "-----" + // 3
                        "-----" + // 2
                        "-☺---" + // 1
                        "-----", // 0
                "-----" + // 4
                        "-----" + // 3
                        "-----" + // 2
                        "-----" + // 1
                        "-----"); // 0
        ASearch search = new ASearch(memory);
        Optional<ASearchRoute> retval = strategy.goToNearGold(search, search.getMemory().getLongMemory().getMe(), 3, 3);
        assertFalse(retval.isPresent());
        retval = strategy.goToNearGold(search, search.getMemory().getLongMemory().getMe(), 4, 4);
        assertTrue(retval.isPresent());
    }
}
