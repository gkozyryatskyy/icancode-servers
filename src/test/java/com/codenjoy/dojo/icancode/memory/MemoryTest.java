package com.codenjoy.dojo.icancode.memory;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Set;

import org.junit.Test;

import com.codenjoy.dojo.icancode.BaseBoardTest;
import com.codenjoy.dojo.icancode.asearch.command.Move;
import com.codenjoy.dojo.icancode.asearch.model.Cell;

public class MemoryTest extends BaseBoardTest {

    @Test
    public void getMapBordersTest() {
        Memory memory = board(defaultBoard);
        Set<Cell> borders = memory.getMapBorders();
        assertEquals(memory.getSize() * 4 - 4, borders.size());
    }

    @Test
    public void getMapLeftBordersTest() {
        Memory memory = board(defaultBoard);
        Set<Cell> borders = memory.getMapBorder(Move.LEFT);
        assertEquals(memory.getSize(), borders.size());
        assertTrue(borders.stream().allMatch(e -> e.getX() == 0));
    }

    @Test
    public void getMapRightBordersTest() {
        Memory memory = board(defaultBoard);
        Set<Cell> borders = memory.getMapBorder(Move.RIGHT);
        assertEquals(memory.getSize(), borders.size());
        assertTrue(borders.stream().allMatch(e -> e.getX() == memory.getSize() - 1));
    }

    @Test
    public void getMapDownBordersTest() {
        Memory memory = board(defaultBoard);
        Set<Cell> borders = memory.getMapBorder(Move.DOWN);
        assertEquals(memory.getSize(), borders.size());
        assertTrue(borders.stream().allMatch(e -> e.getY() == 0));
    }

    @Test
    public void getMapUpBordersTest() {
        Memory memory = board(defaultBoard);
        Set<Cell> borders = memory.getMapBorder(Move.UP);
        assertEquals(memory.getSize(), borders.size());
        assertTrue(borders.stream().allMatch(e -> e.getY() == memory.getSize() - 1));
    }

}
